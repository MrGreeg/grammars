#Главная программа
import tkinter
import random
from time import sleep, time

import networkx as nx
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
# import hashlib
#
# m = hashlib.sha512("".encode('utf-8'))
# m.hexdigest()

# Функция преобразовывает задание из одного формата в другой
# Входные параметры: string - строка для преобразования
# Возвращаемый результат: Кортеж, состоящий из кортежа состояний, кортежа воздействий, кортежа правил
def format_translation(string):
    #Пример "S,A,B,C/a,b,c,#/S:C#/C:Ac|Cb|Ad|c/B:Cb|Ba|b|c/A:Ba|Bd|a|d"
    elements = string.split("/")
    states = elements[0].split(",")
    states.insert(0,'H')
    elements.pop(0)
    signals = elements[0].split(",")
    elements.pop(0)
    rules = []
    for i in elements:
        result = i.split(":")
        for j in result[1].split("|"):
            if len(j) == 1:
                rules.append(("H", j, result[0]))
            else:
                rules.append((j[0],j[1],result[0]))
    return (tuple(states), tuple(signals), tuple(rules))

def task_generation(count_states,count_signals,count_rules):
    all_states = list(map(chr,list(range(ord('A'),ord('Z')+1))))
    all_states.remove('S')
    all_states.remove('H')
    all_signals = list(map(chr,list(range(ord('a'),ord('z')+1))))
    format2 = {}
    result2 = "S"
    format2['S'] = ""
    states = ['H','S']
    signals = ['#']
    rules = []
    for i in range(count_states-2):
        state = all_states.pop(random.randint(0,len(all_states)-2))
        states.append(state)
        format2[state] = ""
        result2 += ","+state
    result2 += "/#"
    for i in range(count_signals-1):
        signal = all_signals.pop(random.randint(0,len(all_signals)-1))
        signals.append(signal)
        result2 += ","+signal
    result2 += "/"
    rules.append(('H',signals[1],states[2]))
    rules.append((states[2], '#', 'S'))
    all_rules = []                      #будет содержать все допустимые переходы
    for i in states:
        if i == 'S': continue
        for j in signals[1:] if i=='H' else signals:
            if j=='#':
                all_rules.append((i,j,'S'))
            else:
                for k in states[2:]:
                    all_rules.append((i,j,k))
    all_rules.remove(rules[0])
    all_rules.remove(rules[1])
    format2[rules[0][2]] += rules[0][1]+"|"
    format2[rules[1][2]] += rules[1][0] + rules[1][1] + "|"
    for i in range(len(all_rules)):
        j = random.randint(0, len(all_rules) - 1)
        all_rules[i], all_rules[j] = all_rules[j], all_rules[i]

    all_states = states[2:]
    all_signals = signals[1:]
    count_rules_x = int(0.4 * count_rules)
    rules_on_state = int(count_rules_x / len(all_states))
    if rules_on_state < 2:
        rules_on_state=2
    while (len(rules)-2) < count_rules_x:
        state = all_states.pop(random.randint(0,len(all_states)-1))
        signal = all_signals.pop(random.randint(0,len(all_signals)-1))
        for j in range(rules_on_state):
            rules_repeat = list(filter(lambda x: x[0]==state and x[1]==signal,all_rules))
            rule = rules_repeat[random.randint(0,len(rules_repeat)-1)]
            all_rules.remove(rule)
            rules.append(rule)
            if rule[0] == 'H':
                format2[rule[2]] += rule[1] + "|"
            else:
                format2[rule[2]] += rule[0] + rule[1] + "|"

    for i in range(count_rules-len(rules)):
        rule = all_rules.pop(random.randint(0, len(all_rules) - 1))
        rules.append(rule)
        if rule[0] == 'H':
            format2[rule[2]] += rule[1] + "|"
        else:
            format2[rule[2]] += rule[0] + rule[1] + "|"
    for i in format2:
        result2 += i+":"+format2[i][:len(format2[i]) - 1]+"/"
    result2 = result2[:len(result2)-1]

    return (states, signals, rules), result2

def graph(task):
    G = nx.MultiDiGraph()
    G.graph['graph'] = {'ratio': 1}
    for i in task[0]:
        if i=="H":
            G.add_node(i ,shape="circle", style="filled", fillcolor="green", fontcolor="white")
        elif i=="S":
            G.add_node(i, shape="circle", style="filled", fillcolor="red", fontcolor="white")
        else:
            G.add_node(i, shape="circle", style="filled", fillcolor="blue", fontcolor="white")
    rules = task[2].copy()
    while rules != []:
        rules_x = list(filter(lambda x: x[0]==rules[0][0] and x[2]==rules[0][2],rules))
        state_from = rules[0][0]
        state_to = rules[0][2]
        if len(rules_x) > 1:
            string = '"'
            for i in rules_x:
                string += i[1]+","
                rules.remove(i)
            string = string[:len(string) - 1]+'"'
        else:
            string = ""
            string += rules_x[0][1]
            rules.remove(rules_x[0])
        G.add_edge(state_from, state_to, label=string)
    nx.drawing.nx_pydot.write_dot(G, "1.dot")
    command = r'"%s/graphviz/bin/dot.exe" -T png 1.dot > 1.png' % (os.getcwd().replace('\\','/'))
    if os.system(command) == 0:
        image = mpimg.imread("1.png")
        plt.imshow(image)
        plt.show()
    else:
        print("Ощибка отрисовки графа")
    command = r'"%s/graphviz/bin/dot.exe" -T pdf 1.dot > 1.pdf' % (os.getcwd().replace('\\', '/'))
    os.system(command)



class class_windows_main:
    def __init__(self,root):
        self.windows = root
        self.menubar = tkinter.Menu(self.windows)
        self.windows.config(menu=self.menubar)
        self.fileMenu = tkinter.Menu(self.menubar)
        self.fileMenu.add_command(label="Открыть", command=self.onExit)
        self.fileMenu.add_command(label="Сохранить", command=self.onExit)
        self.fileMenu.add_command(label="Выход", command=self.onExit)
        self.menubar.add_cascade(label="Файл", menu=self.fileMenu)

    def onExit(self):
        self.windows.quit()

if __name__ == '__main__':
    # root = tkinter.Tk()
    # root.geometry("900x600+300+300")
    # root.title("Грамматики")
    # windows_main = class_windows_main(root)
    # top = tkinter.Toplevel(windows_main.windows)
    # top.transient(windows_main.windows)
    task_format1, task_format2 = task_generation(5,5,16)
    print(task_format2)
    graph(task_format1)

    format_translation("S,A,B,C/a,b,c,#/S:C#/C:Ac|Cb|Ad|c/B:Cb|Ba|b|c/A:Ba|Bd|a|d")
    # root.mainloop()