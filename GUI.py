import itertools
import os
import re
import sys

import networkx as nx
import wx
from test_questions import questions
import helps
import random
import bcrypt
from wx import html2
from wx.html2 import WebView
from wx import grid
from wx import adv

def format_translation(string):
    #Пример "S,A,B,C/a,b,c,#/S:C#/C:Ac|Cb|Ad|c/B:Cb|Ba|b|c/A:Ba|Bd|a|d"
    elements = string.split("/")
    states = elements[0].split(",")
    states.insert(0,'H')
    elements.pop(0)
    signals = elements[0].split(",")
    elements.pop(0)
    rules = []
    for i in elements:
        result = i.split(":")
        for j in result[1].split("|"):
            if len(j) == 1:
                rules.append(("H", j, result[0]))
            else:
                rules.append((j[0],j[1],result[0]))
    return (tuple(states), tuple(signals), tuple(rules))

def graph(task, filename):
    G = nx.MultiDiGraph()
    #G.graph['graph'] = {'ratio': 1}
    for i in task[0]:
        if i=="H":
            G.add_node(i ,shape="circle", style="filled", fillcolor="green", fontcolor="white")
        elif i=="S":
            G.add_node(i, shape="circle", style="filled", fillcolor="red", fontcolor="white")
        else:
            G.add_node(i, shape="circle", style="filled", fillcolor="blue", fontcolor="white")
    rules = list(task[2])
    while rules != []:
        rules_x = list(filter(lambda x: x[0]==rules[0][0] and x[2]==rules[0][2],rules))
        state_from = rules[0][0]
        state_to = rules[0][2]
        if len(rules_x) > 1:
            string = '"'
            for i in rules_x:
                string += i[1]+","
                rules.remove(i)
            string = string[:len(string) - 1]+'"'
        else:
            string = ""
            string += rules_x[0][1]
            rules.remove(rules_x[0])
        G.add_edge(state_from, state_to, label=string)
    nx.drawing.nx_pydot.write_dot(G, "temp.dot")
    command = r'"%s/graphviz/bin/dot.exe" -T svg temp.dot > %s' % (os.getcwd().replace('\\','/'), filename)
    print(command)
    if os.system(command) == 0:
        #os.remove("temp.dot")
        return 0
    else:
        print("Ощибка отрисовки графа")
        return 1

class All_data:
    saved = True
    filename = ""
    FIO=""
    Group=""
    Test_True_questions = 0
    Test_False_questions = 0
    task_states = []
    task_signals = []
    task_rules = []
    task_all = ""

    step1_convolution_table = []
    step1_convolution_table_status = False
    step1_jump_table = []
    step1_jump_table_status = False

    step2_set_states = ""
    step2_set_states_status = False
    step2_input_signal_set = ""
    step2_input_signal_set_status = False
    step2_initial_state = ""
    step2_initial_state_status = False
    step2_set_final_states = ""
    step2_set_final_states_status = False
    step2_transition_functions = []
    step2_transition_functions_status = False

    step3_set_states = []
    step3_set_states_status = False
    step3_input_signal_set = ""
    step3_input_signal_set_status = False
    step3_initial_state = ""
    step3_initial_state_status = False
    step3_set_final_states = []
    step3_set_final_states_status = False
    step3_transition_functions = []
    step3_transition_functions_status = False
    step3_delete_states = []
    step3_delete_states_status = False

    step4_states = []
    step4_rules = []
    step4_signals = []
    step4_status = False

    graph_svg1 = ""
    graph_svg2 = ""

    def reset(self):
        self.saved = False
        self.filename = ""
        self.FIO = ""
        self.Group = ""
        self.Test_True_questions = 0
        self.Test_False_questions = 0
        self.task_states = []
        self.task_signals = []
        self.task_rules = []
        self.task_all = ""

        self.step1_convolution_table = []
        self.step1_convolution_table_status = False
        self.step1_jump_table = []
        self.step1_jump_table_status = False

        self.step2_set_states = ""
        self.step2_set_states_status = False
        self.step2_input_signal_set = ""
        self.step2_input_signal_set_status = False
        self.step2_initial_state = ""
        self.step2_initial_state_status = False
        self.step2_set_final_states = ""
        self.step2_set_final_states_status = False
        self.step2_transition_functions = []
        self.step2_transition_functions_status = False

        self.step3_set_states = []
        self.step3_set_states_status = False
        self.step3_input_signal_set = ""
        self.step3_input_signal_set_status = False
        self.step3_initial_state = ""
        self.step3_initial_state_status = False
        self.step3_set_final_states = []
        self.step3_set_final_states_status = False
        self.step3_transition_functions = []
        self.step3_transition_functions_status = False
        self.step3_delete_states = []
        self.step3_delete_states_status = False

        self.step4_states = []
        self.step4_rules = []
        self.step4_signals = []
        self.step4_status = False

        self.graph_svg1 = ""
        self.graph_svg2 = ""

    def save(self, filename):
        result = {}
        result['FIO']=(self.FIO, bcrypt.hashpw(self.FIO.replace(' ','').encode('utf8'), bcrypt.gensalt()))
        result['Group'] = (self.Group, bcrypt.hashpw(self.Group.replace(' ','').encode('utf8'), bcrypt.gensalt()))
        result['Test_True_questions'] = (self.Test_True_questions, bcrypt.hashpw(str(self.Test_True_questions).encode('utf8'), bcrypt.gensalt()))
        result['Test_False_questions'] = (self.Test_False_questions, bcrypt.hashpw(str(self.Test_False_questions).encode('utf8'), bcrypt.gensalt()))
        result['Task'] = (self.task_all, bcrypt.hashpw(str(self.task_all).encode('utf8'), bcrypt.gensalt()))

        result['step1_convolution_table'] = (self.step1_convolution_table, self.step1_convolution_table_status, bcrypt.hashpw((str(self.step1_convolution_table).replace(' ','')+str(self.step1_convolution_table_status)).encode('utf8'), bcrypt.gensalt()))
        result['step1_jump_table'] = (self.step1_jump_table, self.step1_jump_table_status, bcrypt.hashpw((str(self.step1_jump_table).replace(' ','')+str(self.step1_jump_table_status)).encode('utf8'), bcrypt.gensalt()))

        result['step2_set_states'] = (self.step2_set_states, self.step2_set_states_status, bcrypt.hashpw((str(self.step2_set_states).replace(' ','')+str(self.step2_set_states_status)).encode('utf8'), bcrypt.gensalt()))
        result['step2_input_signal_set'] = (self.step2_input_signal_set, self.step2_input_signal_set_status, bcrypt.hashpw((str(self.step2_input_signal_set).replace(' ','')+str(self.step2_input_signal_set_status)).encode('utf8'), bcrypt.gensalt()))
        result['step2_initial_state'] = (self.step2_initial_state, self.step2_initial_state_status, bcrypt.hashpw((str(self.step2_initial_state).replace(' ','')+str(self.step2_initial_state_status)).encode('utf8'), bcrypt.gensalt()))
        result['step2_set_final_states'] = (self.step2_set_final_states, self.step2_set_final_states_status, bcrypt.hashpw((str(self.step2_set_final_states).replace(' ','')+str(self.step2_set_final_states_status)).encode('utf8'), bcrypt.gensalt()))
        result['step2_transition_functions'] = (self.step2_transition_functions, self.step2_transition_functions_status, bcrypt.hashpw((str(self.step2_transition_functions).replace(' ','')+str(self.step2_transition_functions_status)).encode('utf8'), bcrypt.gensalt()))

        result['step3_set_states'] = (self.step3_set_states, self.step3_set_states_status, bcrypt.hashpw((str(self.step3_set_states).replace(' ','')+str(self.step3_set_states_status)).encode('utf8'), bcrypt.gensalt()))
        result['step3_input_signal_set'] = (self.step3_input_signal_set, self.step3_input_signal_set_status, bcrypt.hashpw((str(self.step3_input_signal_set).replace(' ','')+str(self.step3_input_signal_set_status)).encode('utf8'), bcrypt.gensalt()))
        result['step3_initial_state'] = (self.step3_initial_state, self.step3_initial_state_status, bcrypt.hashpw((str(self.step3_initial_state).replace(' ','')+str(self.step3_initial_state_status)).encode('utf8'), bcrypt.gensalt()))
        result['step3_set_final_states'] = (self.step3_set_final_states, self.step3_set_final_states_status, bcrypt.hashpw((str(self.step3_set_final_states).replace(' ','')+str(self.step3_set_final_states_status)).encode('utf8'), bcrypt.gensalt()))
        result['step3_transition_functions'] = (self.step3_transition_functions, self.step3_transition_functions_status, bcrypt.hashpw((str(self.step3_transition_functions).replace(' ','')+str(self.step3_transition_functions_status)).encode('utf8'), bcrypt.gensalt()))
        result['step3_delete_states'] = (self.step3_delete_states, self.step3_delete_states_status, bcrypt.hashpw((str(self.step3_delete_states).replace(' ','')+str(self.step3_delete_states_status)).encode('utf8'), bcrypt.gensalt()))

        result['step4_all'] = (self.step4_states, self.step4_signals, self.step4_rules, self.step4_status, bcrypt.hashpw((str(self.step4_states).replace(' ','')+str(self.step4_signals).replace(' ','')+str(self.step4_rules).replace(' ','')+str(self.step4_status)).encode('utf8'), bcrypt.gensalt()))

        result['graph_svg1'] = self.graph_svg1
        result['graph_svg2'] = self.graph_svg2

        f = open(filename, 'w')
        s = str(result)
        f.write(str((s, bcrypt.hashpw(s.encode('utf8'), bcrypt.gensalt()))))
        f.close()
        self.saved=True
        wx.MessageDialog(wnd, "Проект сохранён", "Сохранение проекта").ShowModal()

    def test_open(self, result):
        flag=True
        if not bcrypt.checkpw(result['FIO'][0].replace(' ','').encode('utf8'), result['FIO'][1]):
            errors.text.write('Изменено ФИО.\n')
            flag=False
        elif not bcrypt.checkpw(result['Group'][0].replace(' ','').encode('utf8'), result['Group'][1]):
            errors.text.write('Изменена учебная группа.\n')
            flag = False
        elif not bcrypt.checkpw(str(result['Test_True_questions'][0]).encode('utf8'),
                                result['Test_True_questions'][1]):
            errors.text.write('Изменено количество верных ответов на тест.\n')
            flag = False
        elif not bcrypt.checkpw(str(result['Test_False_questions'][0]).encode('utf8'),
                                result['Test_False_questions'][1]):
            errors.text.write('Изменено количество неверных ответов на тест.\n')
            flag = False
        elif not bcrypt.checkpw(str(result['Task'][0]).encode('utf8'), result['Task'][1]):
            errors.text.write('Изменено задание.\n')
            flag = False
        elif not bcrypt.checkpw((str(result['step1_convolution_table'][0]).replace(' ','') + str(
                result['step1_convolution_table'][1])).encode('utf8'),
                                result['step1_convolution_table'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 1: таблица свёрток.\n')
            flag = False
        elif not bcrypt.checkpw(
                (str(result['step1_jump_table'][0]).replace(' ','') + str(result['step1_jump_table'][1])).encode('utf8'),
                result['step1_jump_table'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 1: таблица переходов.\n')
            flag = False
        elif not bcrypt.checkpw(
                (str(result['step2_set_states'][0]).replace(' ','') + str(result['step2_set_states'][1])).encode('utf8'),
                result['step2_set_states'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: множество состояний.\n')
            flag = False
        elif not bcrypt.checkpw((str(result['step2_input_signal_set'][0]).replace(' ','') + str(
                result['step2_input_signal_set'][1])).encode('utf8'), result['step2_input_signal_set'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: множество входных сигналов.\n')
            flag = False
        elif not bcrypt.checkpw(
                (str(result['step2_initial_state'][0]).replace(' ','') + str(result['step2_initial_state'][1])).encode(
                    'utf8'), result['step2_initial_state'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: начальное состояние.\n')
            flag = False
        elif not bcrypt.checkpw((str(result['step2_set_final_states'][0]).replace(' ','') + str(
                result['step2_set_final_states'][1])).encode('utf8'), result['step2_set_final_states'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: множество конечных состояний.\n')
            flag = False
        elif not bcrypt.checkpw((str(result['step2_transition_functions'][0]).replace(' ','') + str(
                result['step2_transition_functions'][1])).encode('utf8'),
                                result['step2_transition_functions'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: функции переходов.\n')
            flag = False

        elif not bcrypt.checkpw(
                (str(result['step3_set_states'][0]).replace(' ','') + str(result['step3_set_states'][1])).encode('utf8'),
                result['step3_set_states'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: множество состояний.\n')
            flag = False
        elif not bcrypt.checkpw((str(result['step3_input_signal_set'][0]).replace(' ','') + str(
                result['step3_input_signal_set'][1])).encode('utf8'), result['step3_input_signal_set'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: множество входных сигналов.\n')
            flag = False
        elif not bcrypt.checkpw(
                (str(result['step3_initial_state'][0]).replace(' ','') + str(result['step3_initial_state'][1])).encode(
                    'utf8'), result['step3_initial_state'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: начальное состояние.\n')
            flag = False
        elif not bcrypt.checkpw((str(result['step3_set_final_states'][0]).replace(' ','') + str(
                result['step3_set_final_states'][1])).encode('utf8'), result['step3_set_final_states'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: множество конечных состояний.\n')
            flag = False
        elif not bcrypt.checkpw((str(result['step3_transition_functions'][0]).replace(' ','') + str(
                result['step3_transition_functions'][1])).encode('utf8'),
                                result['step3_transition_functions'][2]):
            errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: функции переходов.\n')
            flag = False
        elif not bcrypt.checkpw(
                (str(result['step3_delete_states'][0]).replace(' ','') + str(result['step3_delete_states'][1])).encode(
                    'utf8'), result['step3_delete_states'][2]):
            errors.text.write(
                'Изменён резуьтат выполнения заданий: шаг 3: множество недостижимых состояний.\n')
            flag = False
        # if flag==False: wx.MessageDialog(wnd, "Ошибка при открытии проекта. Подробнее в окне ошибок.", "Открытие проекта").ShowModal()
        return flag

    def open(self, filename):
        errors.text.Clear()
        try:
            f = open(filename, 'r')
            s = f.read()
            f.close()
            all_data.reset()
            result = eval(s)
            if bcrypt.checkpw(result[0].encode('utf8'), result[1]):
                result = eval(result[0])
                if self.test_open(result)==True:
                    self.FIO = result['FIO'][0]
                    self.Group = result['Group'][0]
                    wnd.SetTitle("Лабораторная работа: Грамматики    Студент: %s,   Группа: %s"%(self.FIO,self.Group))
                    self.Test_True_questions = result['Test_True_questions'][0]
                    self.Test_False_questions = result['Test_False_questions'][0]
                    self.task_all = result['Task'][0]
                    self.task_states, self.task_signals, self.task_rules = format_translation(self.task_all)
                    wnd.menu_step.Enable(5, True)
                    wnd.menu_step.Enable(6, True)
                    wnd.menu_step.Enable(7, True)
                    self.step1_convolution_table = result['step1_convolution_table'][0]
                    self.step1_convolution_table_status = result['step1_convolution_table'][1]
                    self.step1_jump_table = result['step1_jump_table'][0]
                    self.step1_jump_table_status = result['step1_jump_table'][1]

                    if self.step1_convolution_table_status==True and self.step1_jump_table_status==True:
                        wnd.menu_step.Enable(8, True)
                        wnd.menu_step.Enable(9, True)
                    self.step2_set_states = result['step2_set_states'][0]
                    self.step2_set_states_status = result['step2_set_states'][1]
                    self.step2_input_signal_set = result['step2_input_signal_set'][0]
                    self.step2_input_signal_set_status = result['step2_input_signal_set'][1]
                    self.step2_initial_state = result['step2_initial_state'][0]
                    self.step2_initial_state_status = result['step2_initial_state'][1]
                    self.step2_set_final_states = result['step2_set_final_states'][0]
                    self.step2_set_final_states_status = result['step2_set_final_states'][1]
                    self.step2_transition_functions = result['step2_transition_functions'][0]
                    self.step2_transition_functions_status = result['step2_transition_functions'][1]

                    if self.step2_set_states_status==True and self.step2_input_signal_set_status==True and \
                            self.step2_initial_state_status==True and self.step2_set_final_states_status==True and \
                            self.step2_transition_functions_status==True:
                        wnd.menu_step.Enable(10, True)
                    self.step3_set_states = result['step3_set_states'][0]
                    self.step3_set_states_status = result['step3_set_states'][1]
                    self.step3_input_signal_set = result['step3_input_signal_set'][0]
                    self.step3_input_signal_set_status = result['step3_input_signal_set'][1]
                    self.step3_initial_state = result['step3_initial_state'][0]
                    self.step3_initial_state_status = result['step3_initial_state'][1]
                    self.step3_set_final_states = result['step3_set_final_states'][0]
                    self.step3_set_final_states_status = result['step3_set_final_states'][1]
                    self.step3_transition_functions = result['step3_transition_functions'][0]
                    self.step3_transition_functions_status = result['step3_transition_functions'][1]
                    self.step3_delete_states = result['step3_delete_states'][0]
                    self.step3_delete_states_status = result['step3_delete_states'][1]
                    if self.step3_set_states_status==True and self.step3_input_signal_set_status==True and \
                            self.step3_initial_state_status==True and self.step3_set_final_states_status==True and \
                            self.step3_transition_functions_status==True and self.step3_delete_states_status==True:
                        wnd.menu_step.Enable(11, True)

                    self.step4_states = result['step4_all'][0]
                    self.step4_signals = result['step4_all'][1]
                    self.step4_rules = result['step4_all'][2]
                    self.step4_status = result['step4_all'][3]
                    if self.step4_status==True:
                        wnd.menu_step.Enable(12, True)

                    self.graph_svg1 = result['graph_svg1']
                    self.graph_svg2 = result['graph_svg2']
                    self.saved=True
                    wx.MessageDialog(wnd, "Проект открыт", "Открытие проекта").ShowModal()
                else:
                    wnd.menu_file.Enable(2, False)
                    wnd.menu_file.Enable(3, False)
                    wx.MessageDialog(wnd, "Ошибка при открытии проекта. Подробнее в окне ошибок.",
                                     "Открытие проекта").ShowModal()
            else:
                errors.text.Clear()
                wnd.menu_file.Enable(2, False)
                wnd.menu_file.Enable(3, False)
                errors.text.write("Обнаружено изменение в файле проекта!!!\n")
                try:
                    result = eval(result[0])
                    if not bcrypt.checkpw(result['FIO'][0].replace(' ','').encode('utf8'), result['FIO'][1]):
                        errors.text.write('Изменено ФИО.\n')
                    elif not bcrypt.checkpw(result['Group'][0].replace(' ','').encode('utf8'), result['Group'][1]):
                        errors.text.write('Изменена учебная группа.\n')
                    elif not bcrypt.checkpw(str(result['Test_True_questions'][0]).replace(' ','').encode('utf8'),
                                            result['Test_True_questions'][1]):
                        errors.text.write('Изменено количество верных ответов на тест.\n')
                    elif not bcrypt.checkpw(str(result['Test_False_questions'][0]).replace(' ','').encode('utf8'),
                                            result['Test_False_questions'][1]):
                        errors.text.write('Изменено количество неверных ответов на тест.\n')
                    elif not bcrypt.checkpw(str(result['Task'][0]).replace(' ','').encode('utf8'), result['Task'][1]):
                        errors.text.write('Изменено задание.\n')

                    elif not bcrypt.checkpw((str(result['step1_convolution_table'][0]).replace(' ','') + str(
                            result['step1_convolution_table'][1])).encode('utf8'),
                                            result['step1_convolution_table'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 1: таблица свёрток.\n')
                    elif not bcrypt.checkpw(
                            (str(result['step1_jump_table'][0]).replace(' ','') + str(result['step1_jump_table'][1])).encode('utf8'),
                            result['step1_jump_table'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 1: таблица переходов.\n')

                    elif not bcrypt.checkpw(
                            (str(result['step2_set_states'][0]).replace(' ','') + str(result['step2_set_states'][1])).encode('utf8'),
                            result['step2_set_states'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: множество состояний.\n')
                    elif not bcrypt.checkpw((str(result['step2_input_signal_set'][0]).replace(' ','') + str(
                            result['step2_input_signal_set'][1])).encode('utf8'), result['step2_input_signal_set'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: множество входных сигналов.\n')
                    elif not bcrypt.checkpw(
                            (str(result['step2_initial_state'][0]).replace(' ','') + str(result['step2_initial_state'][1])).encode(
                                    'utf8'), result['step2_initial_state'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: начальное состояние.\n')
                    elif not bcrypt.checkpw((str(result['step2_set_final_states'][0]).replace(' ','') + str(
                            result['step2_set_final_states'][1])).encode('utf8'), result['step2_set_final_states'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: множество конечных состояний.\n')
                    elif not bcrypt.checkpw((str(result['step2_transition_functions'][0]).replace(' ','') + str(
                            result['step2_transition_functions'][1])).encode('utf8'),
                                            result['step2_transition_functions'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 2: функции переходов.\n')

                    elif not bcrypt.checkpw(
                            (str(result['step3_set_states'][0]).replace(' ','') + str(result['step3_set_states'][1])).encode('utf8'),
                            result['step3_set_states'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: множество состояний.\n')
                    elif not bcrypt.checkpw((str(result['step3_input_signal_set'][0]).replace(' ','') + str(
                            result['step3_input_signal_set'][1])).encode('utf8'), result['step3_input_signal_set'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: множество входных сигналов.\n')
                    elif not bcrypt.checkpw(
                            (str(result['step3_initial_state'][0]).replace(' ','') + str(result['step3_initial_state'][1])).encode(
                                    'utf8'), result['step3_initial_state'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: начальное состояние.\n')
                    elif not bcrypt.checkpw((str(result['step3_set_final_states'][0]).replace(' ','') + str(
                            result['step3_set_final_states'][1])).encode('utf8'), result['step3_set_final_states'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: множество конечных состояний.\n')
                    elif not bcrypt.checkpw((str(result['step3_transition_functions'][0]).replace(' ','') + str(
                            result['step3_transition_functions'][1])).encode('utf8'),
                                            result['step3_transition_functions'][2]):
                        errors.text.write('Изменён резуьтат выполнения заданий: шаг 3: функции переходов.\n')
                    elif not bcrypt.checkpw(
                            (str(result['step3_delete_states'][0]).replace(' ','') + str(result['step3_delete_states'][1])).encode(
                                    'utf8'), result['step3_delete_states'][2]):
                        errors.text.write(
                            'Изменён резуьтат выполнения заданий: шаг 3: множество недостижимых состояний.\n')
                except:
                    errors.text.write("Повреждение структуры файла проекта!!!\n")
                wx.MessageDialog(wnd, "Ошибка при открытии проекта. Подробнее в окне ошибок.", "Открытие проекта").ShowModal()
        except:
            errors.text.write("Произошла ошибка при открытии файла проекта!!!\n")

all_data = All_data()

class Window_Manager:
    windows = {}
    count = 0
    def add_window(self, window):
        self.windows.update([(str(self.count), window)])
        self.count+=1
        return self.count-1

    def del_window(self, id):
        self.windows.pop(str(id))

    def close_all_windows(self):
        for i in self.windows.values():
            i.Destroy()
        self.windows={}
        self.count=0

window_manager = Window_Manager()

class ErrorsWindow(wx.MDIChildFrame):
    def __init__(self):
        wx.MDIChildFrame.__init__(self,wnd, title="Ошибки", size=(300, 200))
        self.SetIcon(icon)
        self.text = wx.TextCtrl(self, style = wx.TE_MULTILINE)
        self.text.SetEditable(False)
        self.EnableCloseButton(False)

def task_generation(count_states,count_signals,count_rules):
    all_states = list(map(chr,list(range(ord('A'),ord('Z')+1))))
    all_states.remove('S')
    all_states.remove('H')
    all_signals = list(map(chr,list(range(ord('a'),ord('z')+1))))
    format2 = {}
    result2 = "S"
    format2['S'] = ""
    states = ['H','S']
    signals = ['#']
    rules = []
    for i in range(count_states-2):
        state = all_states.pop(random.randint(0,len(all_states)-2))
        states.append(state)
        format2[state] = ""
        result2 += ","+state
    result2 += "/#"
    for i in range(count_signals-1):
        signal = all_signals.pop(random.randint(0,len(all_signals)-1))
        signals.append(signal)
        result2 += ","+signal
    result2 += "/"
    rules.append(('H',signals[1],states[2]))
    rules.append((states[2], '#', 'S'))
    all_rules = []                      #будет содержать все допустимые переходы
    for i in states:
        if i == 'S': continue
        for j in signals[1:] if i=='H' else signals:
            if j=='#':
                all_rules.append((i,j,'S'))
            else:
                for k in states[2:]:
                    all_rules.append((i,j,k))
    all_rules.remove(rules[0])
    all_rules.remove(rules[1])
    format2[rules[0][2]] += rules[0][1]+"|"
    format2[rules[1][2]] += rules[1][0] + rules[1][1] + "|"
    for i in range(len(all_rules)):
        j = random.randint(0, len(all_rules) - 1)
        all_rules[i], all_rules[j] = all_rules[j], all_rules[i]

    all_states = states[2:]
    all_signals = signals[1:]
    count_rules_x = int(0.4 * count_rules)
    rules_on_state = int(count_rules_x / len(all_states))
    if rules_on_state < 2:
        rules_on_state=2
    while (len(rules)-2) < count_rules_x:
        state = all_states.pop(random.randint(0,len(all_states)-1))
        signal = all_signals.pop(random.randint(0,len(all_signals)-1))
        for j in range(rules_on_state):
            rules_repeat = list(filter(lambda x: x[0]==state and x[1]==signal,all_rules))
            rule = rules_repeat[random.randint(0,len(rules_repeat)-1)]
            all_rules.remove(rule)
            rules.append(rule)
            if rule[0] == 'H':
                format2[rule[2]] += rule[1] + "|"
            else:
                format2[rule[2]] += rule[0] + rule[1] + "|"

    for i in range(count_rules-len(rules)):
        rule = all_rules.pop(random.randint(0, len(all_rules) - 1))
        rules.append(rule)
        if rule[0] == 'H':
            format2[rule[2]] += rule[1] + "|"
        else:
            format2[rule[2]] += rule[0] + rule[1] + "|"
    for i in format2:
        result2 += i+":"+format2[i][:len(format2[i]) - 1]+"/"
    result2 = result2[:len(result2)-1]

    return (states, signals, rules), result2

class TaskWindow(wx.MDIChildFrame):
    def __init__(self):
        self.id = window_manager.add_window(window=self)
        wx.MDIChildFrame.__init__(self,wnd, title="Задание", size=(300, 200))
        self.SetIcon(icon)
        panel = wx.Panel(self)
        task = all_data.task_all.split('/')
        wx.StaticText(panel, label="G = ({%s}, {%s}, P, S)"%(task[0], task[1]), pos=(10,10), style=wx.ST_NO_AUTORESIZE)
        for i in range(len(task)-2):
            wx.StaticText(panel, label=task[i+2], pos=(10,25*i+40), style=wx.ST_NO_AUTORESIZE)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.SetMinSize((300, 200))

    def OnClose(self, event):
        wnd.menu_step.Enable(6, True)
        window_manager.del_window(self.id)
        self.Destroy()

class TestResultWindow(wx.MDIChildFrame):
    def __init__(self):
        self.id = window_manager.add_window(window=self)
        wx.MDIChildFrame.__init__(self, wnd, title="Шаг 0: Тест", size=(220, 180))
        self.SetIcon(icon)
        panel = wx.Panel(self)
        wx.StaticText(panel, label="Студент: "+all_data.FIO, pos=(10,10), style=wx.ST_NO_AUTORESIZE)
        wx.StaticText(panel, label="Группа: " + all_data.Group, pos=(10, 30), style=wx.ST_NO_AUTORESIZE)
        wx.StaticText(panel, label="Результат: \n"
                                   "Верных вопросов: %d (%.2f)\n"
                                   "Неверных вопросов: %d (%.2f)\n"%(all_data.Test_True_questions,
                                                                all_data.Test_True_questions*100/(all_data.Test_True_questions+all_data.Test_False_questions),
                                                                all_data.Test_False_questions,
                                                                all_data.Test_False_questions * 100 / (
                                                                all_data.Test_True_questions + all_data.Test_False_questions)), pos=(10,50), style=wx.ST_NO_AUTORESIZE)
        button = wx.Button(panel, label="Закрыть", pos=(70, 110))
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        button.Bind(wx.EVT_BUTTON, self.OnClose)

    def OnClose(self, event):
        wnd.menu_step.Enable(5, True)
        window_manager.del_window(self.id)
        self.Destroy()

class GraphWindow(wx.MDIChildFrame):
    def __init__(self, graphsvg, task=None, flag=True):
        self.id = window_manager.add_window(window=self)
        self.flag = flag
        if flag:
            wx.MDIChildFrame.__init__(self, wnd, title = "Граф недетерминированного автомата", size = (400,450))
        else:
            wx.MDIChildFrame.__init__(self, wnd, title="Граф детерминированного автомата", size=(400, 450))
        self.SetIcon(icon)
        self.h = WebView.New(self, size=(500,500))
        self.h.EnableContextMenu(False)
        self.h.EnableHistory(False)
        if graphsvg=="":
            if task==None:
                graph((all_data.task_states, all_data.task_signals, all_data.task_rules), "temp.svg")
            else:
                graph(task, "temp.svg")
            f = open("temp.svg", 'r')
            graphsvg += f.read()
            f.close()
            all_data.saved=False
            if flag==True:
                all_data.graph_svg1=graphsvg
            else:
                all_data.graph_svg2 = graphsvg
        else:
            f = open("temp.svg", 'w')
            f.write(graphsvg)
            f.close()

        self.h.LoadURL(os.getcwd().replace('\\','/')+"/temp.svg")
        #os.remove("temp.svg")
        self.Bind(wx.EVT_CLOSE, self.OnClose)

    def OnClose(self, event):
        if self.flag:
            wnd.menu_step.Enable(8, True)
        else:
            wnd.menu_step.Enable(11, True)
        window_manager.del_window(self.id)
        self.Destroy()

reRuleTest = re.compile(r'F\(([A-Z]+),[a-z#]\)=([A-Z]+)')    #комиляция регулярного выражения для ускорения
reRuleParse = re.compile(r'F\(|,|\)=')                      #комиляция регулярного выражения для ускорения

def GetRuleFromString(value):
    if re.fullmatch(reRuleTest, value):
        return re.split(reRuleParse, value)[1:]
    else:
        return None

class Step4Window(wx.MDIChildFrame):
    def __init__(self):
        self.id = window_manager.add_window(window=self)
        wx.MDIChildFrame.__init__(self,wnd, title="Шаг 4: Описание детерминированного автомата", size=(350, 300))
        self.SetIcon(icon)
        panel = wx.Panel(self)
        wx.StaticText(panel, label="А' = (%s, M', F', H, S')"%(str(set(all_data.step4_signals)).replace("'", ""),), pos=(10,10), style=wx.ST_NO_AUTORESIZE)
        wx.StaticText(panel, label="M' = %s" % (str(set(all_data.step4_states)).replace("'", ""),), pos=(10, 40),
                      style=wx.ST_NO_AUTORESIZE)
        wx.StaticText(panel, label="S' = {S}", pos=(10, 70),
                      style=wx.ST_NO_AUTORESIZE)
        for i in range(int(len(all_data.step4_rules)/3)):
            for j in range(min(3,len(all_data.step4_rules)-i*3)):
                k = all_data.step4_rules[i*3+j]
                wx.StaticText(panel, label="F'(%s, %s)=%s"%(k[0], k[1], k[2]), pos=(j*100+10, 25*i+100), style=wx.ST_NO_AUTORESIZE)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.SetMinSize((300, 200))

    def OnClose(self, event):
        wnd.menu_step.Enable(12, True)
        window_manager.del_window(self.id)
        self.Destroy()

class Step3Window(wx.MDIChildFrame):
    class SetStatesWindow(wx.MDIChildFrame):
        def __init__(self, step3_window):
            self.id = window_manager.add_window(window=self)
            self.SelectCell = (-1, -1)
            self.step3_window = step3_window
            wx.MDIChildFrame.__init__(self, wnd, title="Шаг 3: Множество состояний", size=(250, 320))
            self.SetIcon(icon)
            panel = wx.Panel(self)
            self.label = wx.StaticText(panel, label="Множество состояний", pos=(10, 13),
                                       style=wx.ST_NO_AUTORESIZE)
            self.grid = wx.grid.Grid(panel, pos=(10, 30))
            self.grid.CreateGrid(10, 1)
            self.grid.SetSize(120, 250)
            self.grid.SetColLabelValue(0, "Состояние")
            self.grid.SetRowLabelSize(30)
            if all_data.step3_set_states != []:
                for i in range(len(all_data.step3_set_states)):
                    if self.grid.GetNumberRows() - 1 == i:
                        self.grid.InsertRows(i + 1)
                    self.grid.SetCellValue(i, 0, all_data.step3_set_states[i])
            self.button = wx.Button(panel, label="Принять", pos=(10, 260), size=(120, 20))
            self.button.Bind(wx.EVT_BUTTON, self.OnClickButton)
            if all_data.step3_set_states_status:
                self.button.Enable(False)
                self.grid.EnableEditing(False)

            self.grid.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.OnSelectCellGrid)
            self.grid.Bind(wx.EVT_KEY_DOWN, self.OnClickKeyGrid)



            self.Bind(wx.EVT_CLOSE, self.Save)
            self.SetMinSize((250,300))
            self.Bind(wx.EVT_SIZE, self.Resize)

        def Resize(self, event):
            width, height = event.Size
            self.button.SetPosition(((width)/2-70,height-5-60))
            self.label.SetPosition((((width)/2-self.label.GetSize()[0]/2,10)))
            self.grid.SetSize((width-40, height-100))
            event.Skip()

        def OnClickButton(self, event):
            self.Save(None)
            lst_del = []
            errors.text.Clear()
            string = "H"
            for i in range(2, len(all_data.task_states)):
                string += all_data.task_states[i]
            string += "S"
            lst = []
            for i in range(1, len(all_data.task_states) + 1):
                for j in list(itertools.combinations(string, i)):
                    lst.append(''.join(j))

            lst = set(lst)
            print(lst)

            #print("\n\n")
            f=True
            for i in range(self.grid.GetNumberRows()):
                value = self.grid.GetCellValue(i, 0).replace(' ', '')
                if self.grid.GetCellValue(i, 0) == "":
                    lst_del.append(i)
                else:
                    if value in lst:
                        lst.remove(value)
                    else:
                        f=False


            for i in range(-len(lst_del) + 1, 1):
                self.grid.DeleteRows(lst_del[-i])

            if (self.grid.GetNumberRows() == 0):
                self.grid.InsertRows(self.grid.GetNumberRows())

            print(lst)
            if len(lst) == 0 and f==True:
                #print("успех")
                all_data.step3_set_states_status = True
                self.button.Enable(False)
                self.step3_window.entry2.Enable()
                self.step3_window.button2.Enable()
            else:
                errors.text.write("Ошибка! Во множестве состояний\n")

        def OnSelectCellGrid(self, event):
            self.SelectCell = (event.GetRow(), event.GetCol())

        def OnClickKeyGrid(self, event):
            key = event.GetKeyCode()
            if (key == wx.WXK_DOWN or key == wx.WXK_TAB or key == 13 or key == 370) and self.SelectCell[0] == (self.grid.GetNumberRows() - 1) and all_data.step3_set_states_status==False:
                self.grid.InsertRows(self.grid.GetNumberRows())
                self.grid.SetGridCursor(row=self.grid.GetNumberRows() - 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            elif key == wx.WXK_TAB or key == 13 or key == 370:
                self.grid.SetGridCursor(row=self.SelectCell[0] + 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            else:
                event.Skip()

        def Save(self, event):
            if all_data.step3_set_states_status==False:
                all_data.step3_set_states = [self.grid.GetCellValue(i, 0) for i in range(self.grid.GetNumberRows())]
                all_data.saved=False
            if event != None:
                window_manager.del_window(self.id)
                event.Skip()

    class SetFinalStatesWindow(wx.MDIChildFrame):
        def __init__(self, step3_window):
            self.id = window_manager.add_window(window=self)
            wx.MDIChildFrame.__init__(self, wnd, title="Множество конечных состояний", size=(250, 320))
            self.SetIcon(icon)
            self.step3_window = step3_window
            panel = wx.Panel(self)
            self.label = wx.StaticText(panel, label="Множество конечных состояний", pos=(10, 13), style=wx.ST_NO_AUTORESIZE)
            self.grid = wx.grid.Grid(panel, pos=(10, 30))
            self.grid.CreateGrid(10, 1)
            if all_data.step3_set_final_states != []:
                for i in range(len(all_data.step3_set_final_states)):
                    if self.grid.GetNumberRows() - 1 == i:
                        self.grid.InsertRows(i + 1)
                    self.grid.SetCellValue(i, 0, all_data.step3_set_final_states[i])
            self.button1 = wx.Button(panel, label="Заполнить", pos=(270, 12), size=(110, 20))
            self.button1.Bind(wx.EVT_BUTTON, self.OnClickButton1)
            if all_data.step3_set_final_states_status:
                self.button1.Enable(False)
                self.grid.EnableEditing(False)
            self.grid.SetSize(120, 250)
            self.grid.SetColLabelValue(0, "Состояние")
            self.grid.SetRowLabelSize(30)


            self.grid.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.OnSelectCellGrid)
            self.grid.Bind(wx.EVT_KEY_DOWN, self.OnClickKeyGrid)
            self.Bind(wx.EVT_SIZE, self.Resize)
            self.SetMinSize((250,300))

        def Resize(self, event):
            width, height = event.Size
            self.button1.SetPosition(((width)/2-70,height-5-60))
            self.label.SetPosition((((width)/2-self.label.GetSize()[0]/2,10)))
            self.grid.SetSize((width-40, height-100))
            event.Skip()

        def OnClickButton1(self, event):
            self.Save(None)
            lst_del = []
            errors.text.Clear()
            string = "H"
            for i in range(2, len(all_data.task_states)):
                string += all_data.task_states[i]
            string += "S"
            lst = []
            for i in range(1, len(all_data.task_states) + 1):
                for j in list(itertools.combinations(string, i)):
                    lst.append(''.join(j))

            lst = set(filter(lambda x: 'S' in x, lst))

            #print("\n\n")
            f=True
            for i in range(self.grid.GetNumberRows()):
                value = self.grid.GetCellValue(i, 0).replace(' ', '')
                if self.grid.GetCellValue(i, 0) == "":
                    lst_del.append(i)
                else:
                    if value in lst:
                        lst.remove(value)
                    else:
                        f=False

            for i in range(-len(lst_del) + 1, 1):
                self.grid.DeleteRows(lst_del[-i])

            if (self.grid.GetNumberRows() == 0):
                self.grid.InsertRows(self.grid.GetNumberRows())

            # print(lst)
            if len(lst) == 0 and f==True:
                #print("успех")
                all_data.step3_set_final_states_status = True
                self.button1.Enable(False)
                self.step3_window.button5.Enable()
            else:
                errors.text.write("Ошибка! Во множестве конечных состояний\n")

        def Save(self, event):
            if all_data.step3_set_final_states_status==False:
                all_data.step3_set_final_states = [self.grid.GetCellValue(i, 0) for i in range(self.grid.GetNumberRows())]
                all_data.saved=False
            if event != None:
                window_manager.del_window(self.id)
                event.Skip()

        def OnSelectCellGrid(self, event):
            self.SelectCell = (event.GetRow(), event.GetCol())

        def OnClickKeyGrid(self, event):
            key = event.GetKeyCode()
            if (key == wx.WXK_DOWN or key == wx.WXK_TAB or key == 13 or key == 370) and self.SelectCell[0] == (self.grid.GetNumberRows() - 1) and all_data.step3_set_final_states_status==False:
                self.grid.InsertRows(self.grid.GetNumberRows())
                self.grid.SetCellValue(self.grid.GetNumberRows() - 1,0,"")
                self.grid.SetGridCursor(row=self.grid.GetNumberRows() - 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            elif key == wx.WXK_TAB or key == 13 or key == 370:
                self.grid.SetGridCursor(row=self.SelectCell[0] + 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            else:
                event.Skip()

    class SetFunctionsWindow(wx.MDIChildFrame):
        def get_list_rules(self):
            string = "H"
            for i in range(2, len(all_data.task_states)):
                string += all_data.task_states[i]
            string += "S"

            lst = []
            for i in range(1, len(string) + 1):
                for j in list(itertools.combinations(string, i)):
                    lst.append(''.join(j))
            #print('Множество состоянии: ', lst)
            lst_last = list(filter(lambda x: x[0] == 'H', lst))
            lst_last.sort()
            ads = list(filter(lambda x: x[0] != 'H', lst))
            ads.sort()
            lst_last += ads
            lst_last = [(i[0], i[1], "") for i in list(itertools.product(lst_last, all_data.task_signals))]
            return lst_last

        def __init__(self, step3_window):
            self.id = window_manager.add_window(window=self)
            self.SelectCell = (-1,-1)
            self.step3_window = step3_window
            wx.MDIChildFrame.__init__(self, wnd, title="Шаг 3: Функции переходов", size=(250, 320))
            self.SetIcon(icon)
            panel = wx.Panel(self)
            self.label = wx.StaticText(panel, label="Функции переходов", pos=(10, 13),
                                       style=wx.ST_NO_AUTORESIZE)
            self.grid = wx.grid.Grid(panel, pos=(10, 30))
            self.grid.CreateGrid(0, 1)
            self.grid.SetSize(200, 250)
            self.grid.SetColLabelValue(0,"Состояние")
            self.grid.SetRowLabelSize(100)

            if all_data.step3_transition_functions == []:
                all_data.step3_transition_functions = self.get_list_rules()

            for i in range(len(all_data.step3_transition_functions)):
                self.grid.InsertRows(i)
                self.grid.SetRowLabelValue(i, "%d. F(%s, %s)=" % (i+1, all_data.step3_transition_functions[i][0],
                                                                all_data.step3_transition_functions[i][1]))
                self.grid.SetCellValue(i, 0, all_data.step3_transition_functions[i][2])

            self.button = wx.Button(panel, label="Принять", pos=(10, 260), size=(120, 20))
            self.button.Bind(wx.EVT_BUTTON, self.OnClickButton)
            if all_data.step3_transition_functions_status:
                self.button.Enable(False)
                self.grid.EnableEditing(False)

            self.grid.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.OnSelectCellGrid)
            self.grid.Bind(wx.EVT_KEY_DOWN, self.OnClickKeyGrid)



            self.Bind(wx.EVT_CLOSE, self.Save)
            self.Bind(wx.EVT_SIZE, self.Resize)

            self.SetMinSize((250,300))
        def Resize(self, event):
            width, height = event.Size
            self.button.SetPosition(((width)/2-70,height-5-60))
            self.label.SetPosition((((width)/2-self.label.GetSize()[0]/2,10)))
            self.grid.SetSize((width-40, height-100))
            event.Skip()

        def OnClickButton(self, event):
            self.Save(None)
            flags = [True for i in range(self.grid.GetNumberRows())]

            rules = self.get_list_rules()
            rules_for_check = {}
            for i in all_data.task_rules:
                key = rules_for_check.get(i[0]+i[1])
                if (key == None):
                    rules_for_check.setdefault(i[0]+i[1], [i[2]])
                else:
                    rules_for_check[i[0]+i[1]].append(i[2])
            f=True
            for i in range(self.grid.GetNumberRows()):
                value = self.grid.GetCellValue(i,0).replace(' ', '')
                if value == "":
                    flags[i] = False
                    f=False

                    lst_str = []
                    for j in str(rules[i][0]):
                        result = rules_for_check.get(j + str(rules[i][1]))
                        if (result != None):
                            # print(set(lst_str + result))
                            lst_str = list(set(lst_str + result))
                    if (len(lst_str) == 0):
                        lst_str = ['-']
                    else:
                        lst_str.sort()
                    #print(i+1,''.join(lst_str))


                else:
                    value = list(value)
                    value.sort()
                    value = ''.join(value)
                    lst_str = []
                    for j in str(rules[i][0]):
                        result = rules_for_check.get(j + str(rules[i][1]))
                        if (result != None):
                            # print(set(lst_str + result))
                            lst_str = list(set(lst_str + result))
                    if (len(lst_str) == 0):
                        lst_str = ['-']
                    else:
                        lst_str.sort()
                    #print(i+1,''.join(lst_str))
                    if value != ''.join(lst_str):
                        flags[i] = False
                        f = False

            errors.text.Clear()
            for i in range(len(flags)):
                if flags[i] == False:
                    errors.text.write('Неверное значение в строке №' + str(i + 1)+'\n')

            if f == True:
                #print("успех")
                all_data.step3_transition_functions_status = True
                self.grid.EnableEditing(False)
                self.button.Enable(False)
                self.step3_window.button6.Enable()
            else:
                errors.text.write("Обнаружены ошибки в функциях переходов.\n")

        def OnSelectCellGrid(self, event):
            self.SelectCell = (event.GetRow(), event.GetCol())

        def OnClickKeyGrid(self, event):
            key = event.GetKeyCode()
            if (key == wx.WXK_DOWN or key == wx.WXK_TAB or key == 13 or key == 370) and self.SelectCell[0] == (self.grid.GetNumberRows() - 1) and all_data.step3_transition_functions_status==False:
                self.grid.SetCellValue(self.grid.GetNumberRows() - 1,0,"")
                self.grid.SetGridCursor(row=self.grid.GetNumberRows() - 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            elif key == wx.WXK_TAB or key == 13 or key == 370:
                self.grid.SetGridCursor(row=self.SelectCell[0] + 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            else:
                event.Skip()

        def Save(self, event):
            if all_data.step3_transition_functions_status==False:
                rules = self.get_list_rules()
                all_data.step3_transition_functions = []
                for i in range(len(rules)):
                    all_data.step3_transition_functions.append((rules[i][0], rules[i][1], self.grid.GetCellValue(i,0)))
                all_data.saved=False
            if event!=None:
                window_manager.del_window(self.id)
                event.Skip()

    class SetDeleteStatesWindow(wx.MDIChildFrame):
        def getresult(self, string, rules_for_check, state, signal):
            lst_str = []
            for j in str(state):
                result = rules_for_check.get(j + str(signal))
                if (result != None):
                    # print(set(lst_str + result))
                    lst_str = list(set(lst_str + result))
            if (len(lst_str) == 0):
                return '-'
            else:
                lst_str.sort(key=lambda x: string.index(x))
                return ''.join(lst_str)

        def __init__(self, step3_window):
            self.id = window_manager.add_window(window=self)
            wx.MDIChildFrame.__init__(self, wnd, title="Множество недостижимых состояний", size=(250, 320))
            self.SetIcon(icon)
            self.step3_window = step3_window
            panel = wx.Panel(self)
            self.label = wx.StaticText(panel, label="Множество недостижимых состояний", pos=(10, 13), style=wx.ST_NO_AUTORESIZE)
            self.grid = wx.grid.Grid(panel, pos=(10, 30))
            self.grid.CreateGrid(10, 1)
            self.grid.SetSize(200, 220)
            self.grid.SetColLabelValue(0, "Состояние")
            self.grid.SetRowLabelSize(30)
            if all_data.step3_delete_states != []:
                for i in range(len(all_data.step3_delete_states)):
                    if self.grid.GetNumberRows() - 1 == i:
                        self.grid.InsertRows(i + 1)
                    self.grid.SetCellValue(i, 0, all_data.step3_delete_states[i])
            self.button1 = wx.Button(panel, label="Заполнить", pos=(12, 280), size=(110, 20))
            self.button1.Bind(wx.EVT_BUTTON, self.OnClickButton1)
            if all_data.step3_delete_states_status:
                # self.button1.Enable(False)
                self.grid.EnableEditing(False)


            self.grid.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.OnSelectCellGrid)
            self.grid.Bind(wx.EVT_KEY_DOWN, self.OnClickKeyGrid)
            self.Bind(wx.EVT_SIZE, self.Resize)
            self.SetMinSize((250, 300))

        def Resize(self, event):
            width, height = event.Size
            self.button1.SetPosition(((width)/2-70,height-5-60))
            self.label.SetPosition((((width)/2-self.label.GetSize()[0]/2,10)))
            self.grid.SetSize((width-40, height-100))
            event.Skip()

        def OnClickButton1(self, event):
            self.Save(None)
            errors.text.Clear()
            string = "H"
            for i in range(2, len(all_data.task_states)):
                string += all_data.task_states[i]
            string += "S"
            lst_del = []
            lst = []
            for i in range(1, len(string) + 1):
                for j in list(itertools.combinations(string, i)):
                    lst.append(''.join(j))
            print('Множество состоянии: ', lst)
            lst_last = list(filter(lambda x: x[0] == 'H', lst))
            lst_last.sort()
            ads = list(filter(lambda x: x[0] != 'H', lst))
            ads.sort()
            lst_last += ads
            rules_for_check = {}
            for i in all_data.task_rules:
                key = rules_for_check.get(i[0] + i[1])
                if (key == None):
                    rules_for_check.setdefault(i[0] + i[1], [i[2]])
                else:
                    rules_for_check[i[0] + i[1]].append(i[2])
            rules = [(i[0], i[1], self.getresult(string, rules_for_check, i[0], i[1])) for i in list(itertools.product(lst_last, all_data.task_signals))]

            unattainable_states = lst_last.copy()
            attainable_states = set('H')
            for i in rules:
                if i[2] != "-":
                    attainable_states.add(i[2])
            rules_copy = rules.copy()
            rules = list(filter(lambda x: x[2] in attainable_states and x[0] in attainable_states and x[2]!=x[0], rules))
            tostate = [i[2] for i in rules]
            rules = list(filter(lambda x: (x[0] in tostate or x[0]=='H'), rules))
            attainable_states = set('H')
            for i in rules:
                if i[2] != "-":
                    attainable_states.add(i[2])

            rules2 = []
            for i in rules:
                if i[2] != "-" and i[0] in attainable_states:
                    rules2.append(i)
            unattainable_states = set(unattainable_states) - attainable_states
            rules = list(filter(lambda x: x[2] in attainable_states and x[2] == x[0], rules_copy))
            for i in rules:
                rules2.append(i)
            self.rules = rules2
            self.states = list(attainable_states)

            f=True
            for i in range(self.grid.GetNumberRows()):
                value = self.grid.GetCellValue(i, 0).replace(' ', '')
                if value == "":
                    lst_del.append(i)
                else:
                    if value in unattainable_states:
                        unattainable_states.remove(value)
                    else:
                        f=False

            for i in range(-len(lst_del) + 1, 1):
                self.grid.DeleteRows(lst_del[-i])

            if (self.grid.GetNumberRows() == 0):
                self.grid.InsertRows(self.grid.GetNumberRows())
            # print(unattainable_states)
            if len(unattainable_states) == 0 and f==True:
                #print("успех")
                all_data.step3_delete_states_status = True
                self.button1.Enable(False)
                #print((self.states, all_data.task_signals, self.rules))
                GraphWindow(all_data.graph_svg2, (self.states, all_data.task_signals, self.rules), False)
                all_data.step4_states, all_data.step4_signals, all_data.step4_rules, all_data.step4_status = self.states, all_data.task_signals, self.rules, True
                Step4Window()
            else:
                errors.text.write("Ошибка! Во множестве недостижимых состояний\n")

        def Save(self, event):
            if all_data.step3_delete_states_status==False:
                all_data.step3_delete_states = [self.grid.GetCellValue(i, 0) for i in range(self.grid.GetNumberRows())]
                all_data.saved=False
            if event != None:
                window_manager.del_window(self.id)
                event.Skip()

        def OnSelectCellGrid(self, event):
            self.SelectCell = (event.GetRow(), event.GetCol())

        def OnClickKeyGrid(self, event):
            key = event.GetKeyCode()
            if (key == wx.WXK_DOWN or key == wx.WXK_TAB or key == 13 or key == 370) and self.SelectCell[0] == (self.grid.GetNumberRows() - 1) and all_data.step3_delete_states_status==False:
                self.grid.InsertRows(self.grid.GetNumberRows())
                self.grid.SetCellValue(self.grid.GetNumberRows() - 1,0,"")
                self.grid.SetGridCursor(row=self.grid.GetNumberRows() - 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            elif key == wx.WXK_TAB or key == 13 or key == 370:
                self.grid.SetGridCursor(row=self.SelectCell[0] + 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            else:
                event.Skip()

    def __init__(self):
        self.id = window_manager.add_window(window=self)
        wx.MDIChildFrame.__init__(self, wnd, title="Шаг 3: Детерминированный конечный автомат", size=(300, 260),
                                  style=wx.DEFAULT_FRAME_STYLE & ~wx.MAXIMIZE_BOX & ~wx.RESIZE_BORDER)
        self.SetIcon(icon)
        panel = wx.Panel(self)
        wx.StaticText(panel, label="Множество состояний", pos=(10, 13), style=wx.ST_NO_AUTORESIZE)
        self.button1 = wx.Button(panel, label="Заполнить", pos=(140, 12), size=(110, 20))
        self.button1.Bind(wx.EVT_BUTTON, self.OnClickButton1)

        wx.StaticText(panel, label="Множество входных\nсигналов", pos=(10, 43), style=wx.ST_NO_AUTORESIZE|wx.ALIGN_CENTRE_HORIZONTAL)
        self.entry2 = wx.TextCtrl(panel, pos=(140, 45), value=all_data.step3_input_signal_set, style=wx.ST_NO_AUTORESIZE | wx.TE_PROCESS_ENTER)
        self.entry2.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterEntry2)
        self.button2 = wx.Button(panel, label="✓", pos=(253, 46), size=(20, 20))
        self.button2.Bind(wx.EVT_BUTTON, self.OnClickButton2)
        if all_data.step3_set_states_status==False:
            self.entry2.Enable(False)
            self.button2.Enable(False)
        if all_data.step3_input_signal_set_status==True:
            self.entry2.Enable(False)
            self.button2.Enable(False)

        wx.StaticText(panel, label="Начальное состояние", pos=(10, 82), style=wx.ST_NO_AUTORESIZE)
        self.entry3 = wx.TextCtrl(panel, pos=(140, 80), value=all_data.step3_initial_state, style=wx.ST_NO_AUTORESIZE | wx.TE_PROCESS_ENTER)
        self.entry3.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterEntry3)
        self.button3 = wx.Button(panel, label="✓", pos=(253, 81), size=(20, 20))
        self.button3.Bind(wx.EVT_BUTTON, self.OnClickButton3)
        if all_data.step3_input_signal_set_status==False:
            self.entry3.Enable(False)
            self.button3.Enable(False)
        if all_data.step3_initial_state_status == True:
            self.entry3.Enable(False)
            self.button3.Enable(False)

        wx.StaticText(panel, label="Множество конечных\nсостояний", pos=(10, 110), style=wx.ST_NO_AUTORESIZE|wx.ALIGN_CENTRE_HORIZONTAL)
        self.button4 = wx.Button(panel, label="Заполнить", pos=(140, 114), size=(110, 20))
        self.button4.Bind(wx.EVT_BUTTON, self.OnClickButton4)
        if all_data.step3_initial_state_status==False:
            self.button4.Enable(False)

        wx.StaticText(panel, label="Функции переходов", pos=(10, 147), style=wx.ST_NO_AUTORESIZE)
        self.button5 = wx.Button(panel, label="Заполнить", pos=(140, 146), size=(110, 20))
        self.button5.Bind(wx.EVT_BUTTON, self.OnClickButton5)
        if all_data.step3_set_final_states_status==False:
            self.button5.Enable(False)

        wx.StaticText(panel, label="Множество\nнедостижимых\nсостояний", pos=(10, 170), style=wx.ST_NO_AUTORESIZE|wx.ALIGN_CENTRE_HORIZONTAL)
        self.button6 = wx.Button(panel, label="Заполнить", pos=(140, 180), size=(110, 20))
        self.button6.Bind(wx.EVT_BUTTON, self.OnClickButton6)
        if all_data.step3_transition_functions_status==False:
            self.button6.Enable(False)

        self.Bind(wx.EVT_CLOSE, self.OnClose)
        wnd.menu_step.Enable(10, False)

    def OnClose(self, event):
        window_manager.del_window(self.id)
        wnd.menu_step.Enable(10, True)
        event.Skip()

    def OnClickButton1(self, event):
        self.SetStatesWindow(step3_window=self)

    def OnClickButton2(self, event):
        all_data.saved=False
        string = self.entry2.GetValue().replace(',', '').replace(' ', '')
        all_data.step3_input_signal_set = string
        errors.text.Clear()
        #print(set(all_data.task_signals))
        if set(all_data.task_signals) == set(string):
            self.entry2.Enable(False)
            self.button2.Enable(False)
            self.entry3.Enable()
            self.button3.Enable()
            all_data.step3_input_signal_set_status = True
        else:
            errors.text.write("Ошибка! Во множестве входных сигналов\n")

    def OnTextEnterEntry2(self, event):
        self.OnClickButton2(None)

    def OnClickButton3(self, event):
        all_data.saved=False
        string = self.entry3.GetValue().replace(',', '').replace(' ', '')
        all_data.step3_initial_state = string
        errors.text.Clear()
        if string == 'H':
            self.entry3.Enable(False)
            self.button3.Enable(False)
            self.button4.Enable()
            all_data.step3_initial_state_status=True
        else:
            errors.text.write("Ошибка! Неверное начальное состояние\n")

    def OnTextEnterEntry3(self, event):
        self.OnClickButton3(None)

    def OnClickButton4(self, event):
        self.SetFinalStatesWindow(step3_window=self)

    def OnClickButton5(self, event):
        self.SetFunctionsWindow(step3_window=self)

    def OnClickButton6(self, event):
        self.SetDeleteStatesWindow(step3_window=self)

class Step2Window(wx.MDIChildFrame):
    class tableWindow(wx.MDIChildFrame):
        def __init__(self):
            self.id = window_manager.add_window(window=self)

            self.SelectCell = (-1,-1)
            wx.MDIChildFrame.__init__(self, wnd, title="Шаг 2: Недетерминированный конечный автомат", size=(250, 320))
            self.SetIcon(icon)
            panel = wx.Panel(self)
            self.label = wx.StaticText(panel, label="Функции переходов", pos=(10, 13),
                                       style=wx.ST_NO_AUTORESIZE)
            self.grid = wx.grid.Grid(panel, pos=(10, 30))
            self.grid.CreateGrid(10, 1)
            self.grid.SetSize(120, 250)
            self.grid.SetColLabelValue(0,"Функция")
            self.grid.SetRowLabelSize(30)
            for i in range(10):
                self.grid.SetCellValue(i,0,"F(,)=")
            if all_data.step2_transition_functions != []:
                for i in range(len(all_data.step2_transition_functions)):
                    if self.grid.GetNumberRows() - 1 == i:
                        self.grid.InsertRows(i + 1)
                    self.grid.SetCellValue(i, 0, all_data.step2_transition_functions[i])
            self.button = wx.Button(panel, label="Принять", pos=(10, 260), size=(120, 20))
            self.button.Bind(wx.EVT_BUTTON, self.OnClickButton)
            if all_data.step2_transition_functions_status:
                self.button.Enable(False)
                self.grid.EnableEditing(False)

            self.grid.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.OnSelectCellGrid)
            self.grid.Bind(wx.EVT_KEY_DOWN, self.OnClickKeyGrid)

            self.Bind(wx.EVT_CLOSE, self.Save)
            self.Bind(wx.EVT_SIZE, self.Resize)

            self.SetMinSize((250, 300))

        def Resize(self, event):
            width, height = event.Size
            self.button.SetPosition(((width)/2-70,height-5-60))
            self.label.SetPosition((((width)/2-self.label.GetSize()[0]/2,10)))
            self.grid.SetSize((width-40, height-100))
            event.Skip()

        def OnClickButton(self, event):
            self.Save(None)
            lst_del=[]
            flags_error = [True for i in range(self.grid.GetNumberRows())]
            flags = [True for i in range(self.grid.GetNumberRows())]
            lst = set(all_data.task_rules)
            # for i in lst:
            #     print(i)
            #
            # print("\n\n")

            for i in range(self.grid.GetNumberRows()):
                value = self.grid.GetCellValue(i,0).replace(' ', '')
                if self.grid.GetCellValue(i,0) == "" or self.grid.GetCellValue(i,0) == "F(,)=":
                    lst_del.append(i)
                else:
                    result = GetRuleFromString(value)
                    if result != None:
                        if (result[0], result[1], result[2]) in lst:
                            lst.remove((result[0], result[1], result[2]))
                        else:
                            flags[i] = False
                    else:
                        flags_error[i] = False

            for i in range(-len(lst_del) + 1, 1):
                self.grid.DeleteRows(lst_del[-i])
                flags.pop(lst_del[-i])
                flags_error.pop(lst_del[-i])

            if (self.grid.GetNumberRows() == 0):
                self.grid.InsertRows(self.grid.GetNumberRows())
                self.grid.SetCellValue(0, 0, 'F(,)=')
            errors.text.Clear()
            for i in range(len(flags)):
                if flags[i] == False:
                    errors.text.write('Неверное значение в строке №' + str(i + 1)+'\n')

            for i in range(len(flags_error)):
                if flags_error[i] == False:
                    errors.text.write('Некорректное значение в строке №' + str(i + 1)+'\n')

            #print(lst)
            if len(lst) == 0:
                #print("успех")
                all_data.step2_transition_functions_status = True
                self.grid.EnableEditing(False)
                self.button.Enable(False)
                Step3Window()
            else:
                errors.text.write("Определены не все функции переходов\n")

        def OnSelectCellGrid(self, event):
            self.SelectCell = (event.GetRow(), event.GetCol())

        def OnClickKeyGrid(self, event):
            key = event.GetKeyCode()
            if (key == wx.WXK_DOWN or key == wx.WXK_TAB or key == key == 13 or key == 370) and self.SelectCell[0] == (self.grid.GetNumberRows() - 1) and all_data.step2_transition_functions_status==False:
                self.grid.InsertRows(self.grid.GetNumberRows())
                self.grid.SetCellValue(self.grid.GetNumberRows() - 1,0,"F(,)=")
                self.grid.SetGridCursor(row=self.grid.GetNumberRows() - 1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            elif key == wx.WXK_TAB or key == 13 or key == 370:
                self.grid.SetGridCursor(row=self.SelectCell[0]+1, col=0)
                self.grid.EnableCellEditControl()
                self.grid.ShowCellEditControl()
            else: event.Skip()

        def Save(self, event):
            if all_data.step2_transition_functions_status==False:
                all_data.step2_transition_functions = [self.grid.GetCellValue(i,0) for i in range(self.grid.GetNumberRows())]
                all_data.saved=False
            if event!=None:
                window_manager.del_window(self.id)
                event.Skip()

    def __init__(self):
        self.id = window_manager.add_window(window=self)
        wx.MDIChildFrame.__init__(self, wnd, title="Шаг 2: Недетерминированный конечный автомат", size=(300, 215),
                                  style=wx.DEFAULT_FRAME_STYLE & ~wx.MAXIMIZE_BOX & ~wx.RESIZE_BORDER)
        self.SetIcon(icon)
        panel = wx.Panel(self)
        wx.StaticText(panel, label="Множество состояний", pos=(10, 13), style=wx.ST_NO_AUTORESIZE)
        self.entry1 = wx.TextCtrl(panel, pos=(140, 10), value=all_data.step2_set_states, style=wx.ST_NO_AUTORESIZE | wx.TE_PROCESS_ENTER)
        self.entry1.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterEntry1)
        self.button1 = wx.Button(panel, label="✓", pos=(253, 12), size=(20, 20))
        self.button1.Bind(wx.EVT_BUTTON, self.OnClickButton1)
        if all_data.step2_set_states_status==True:
            self.entry1.Enable(False)
            self.button1.Enable(False)

        wx.StaticText(panel, label="Множество входных\n    сигналов", pos=(10, 43), style=wx.ST_NO_AUTORESIZE)
        self.entry2 = wx.TextCtrl(panel, pos=(140, 45), value=all_data.step2_input_signal_set, style=wx.ST_NO_AUTORESIZE| wx.TE_PROCESS_ENTER)
        self.entry2.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterEntry2)
        self.button2 = wx.Button(panel, label="✓", pos=(253, 46), size=(20, 20))
        self.button2.Bind(wx.EVT_BUTTON, self.OnClickButton2)
        if all_data.step2_input_signal_set_status==True:
            self.entry2.Enable(False)
            self.button2.Enable(False)

        wx.StaticText(panel, label="Начальное состояние", pos=(10, 82), style=wx.ST_NO_AUTORESIZE)
        self.entry3 = wx.TextCtrl(panel, pos=(140, 80), value=all_data.step2_initial_state, style=wx.ST_NO_AUTORESIZE| wx.TE_PROCESS_ENTER)
        self.entry3.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterEntry3)
        self.button3 = wx.Button(panel, label="✓", pos=(253, 81), size=(20, 20))
        self.button3.Bind(wx.EVT_BUTTON, self.OnClickButton3)
        if all_data.step2_initial_state_status == True:
            self.entry3.Enable(False)
            self.button3.Enable(False)

        wx.StaticText(panel, label="Множество конечных\n        состояний", pos=(10, 110), style=wx.ST_NO_AUTORESIZE)
        self.entry4 = wx.TextCtrl(panel, pos=(140, 113), value=all_data.step2_set_final_states, style=wx.ST_NO_AUTORESIZE | wx.TE_PROCESS_ENTER)
        self.entry4.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterEntry4)
        self.button4 = wx.Button(panel, label="✓", pos=(253, 114), size=(20, 20))
        self.button4.Bind(wx.EVT_BUTTON, self.OnClickButton4)
        if all_data.step2_set_final_states_status == True:
            self.entry4.Enable(False)
            self.button4.Enable(False)

        wx.StaticText(panel, label="Функции переходов", pos=(10, 147), style=wx.ST_NO_AUTORESIZE)
        self.button5 = wx.Button(panel, label="Заполнить", pos=(140, 146), size=(110, 20))
        self.button5.Bind(wx.EVT_BUTTON, self.OnClickButton5)
        if all_data.step2_set_final_states_status == False:
            self.button5.Enable(False)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        wnd.menu_step.Enable(9, False)

    def OnClose(self, event):
        window_manager.del_window(self.id)
        wnd.menu_step.Enable(9, True)
        event.Skip()

    def OnClickButton1(self, event):
        all_data.saved=False
        string = self.entry1.GetValue().replace(',', '').replace(' ', '')
        all_data.step2_set_states = string
        errors.text.Clear()
        if set(all_data.task_states) == set(string):
            self.entry1.Enable(False)
            self.button1.Enable(False)
            self.entry2.Enable()
            self.button2.Enable()
            all_data.step2_set_states_status = True
        else:
            errors.text.write("Ошибка! Во множестве состояний\n")

    def OnTextEnterEntry1(self, event):
        self.OnClickButton1(None)


    def OnClickButton2(self, event):
        all_data.saved=False
        string = self.entry2.GetValue().replace(',', '').replace(' ', '')
        all_data.step2_input_signal_set = string
        errors.text.Clear()
        if set(all_data.task_signals) == set(string):
            self.entry2.Enable(False)
            self.button2.Enable(False)
            self.entry3.Enable()
            self.button3.Enable()
            all_data.step2_input_signal_set_status = True
        else:
            errors.text.write("Ошибка! Во множестве входных сигналов\n")

    def OnTextEnterEntry2(self, event):
        self.OnClickButton2(None)

    def OnClickButton3(self, event):
        all_data.saved=False
        string = self.entry3.GetValue().replace(',', '').replace(' ', '')
        all_data.step2_initial_state = string
        errors.text.Clear()
        if string == 'H':
            self.entry3.Enable(False)
            self.button3.Enable(False)
            self.entry4.Enable()
            self.button4.Enable()
            all_data.step2_initial_state_status = True
        else:
            errors.text.write("Ошибка! В начальном состоянии\n")

    def OnTextEnterEntry3(self, event):
        self.OnClickButton3(None)

    def OnClickButton4(self, event):
        all_data.saved=False
        string = self.entry4.GetValue().replace(',', '').replace(' ', '')
        all_data.step2_set_final_states = string
        errors.text.Clear()
        if string == 'S':
            self.entry4.Enable(False)
            self.button4.Enable(False)
            self.button5.Enable()
            all_data.step2_set_final_states_status = True
        else:
            errors.text.write("Ошибка! В конечном состоянии\n")

    def OnTextEnterEntry4(self, event):
        self.OnClickButton4(None)

    def OnClickButton5(self, event):
        self.tableWindow()

class Step1Window(wx.MDIChildFrame):
    def __init__(self):
        self.id = window_manager.add_window(window=self)
        self.SelectCell1 = (-1,-1)
        self.SelectCell2 = (-1, -1)
        wx.MDIChildFrame.__init__(self, wnd, title="Шаг 1: Таблицы свёрток и переходов", size=(550, 320),
                                  style=wx.DEFAULT_FRAME_STYLE & ~wx.MAXIMIZE_BOX & ~wx.RESIZE_BORDER)
        self.SetIcon(icon)
        panel = wx.Panel(self)
        self.label1 = wx.StaticText(panel, label="Заполните таблицу свёрток", pos=(10,10), style=wx.ST_NO_AUTORESIZE)
        self.button1 = wx.Button(panel, label="Проверить", pos=(120, 250))
        self.button1.Bind(wx.EVT_BUTTON, self.OnClickButton1)
        self.label2 = wx.StaticText(panel, label="Заполните таблицу переходов", pos=(240, 10), style=wx.ST_NO_AUTORESIZE)
        self.button2 = wx.Button(panel, label="Проверить", pos=(430, 250))
        self.button2.Bind(wx.EVT_BUTTON, self.OnClickButton2)
        self.grid1 = wx.grid.Grid(panel, pos=(10, 40))
        self.grid1.CreateGrid(len(all_data.task_states[1:]), len(all_data.task_signals))
        self.grid1.DisableDragGridSize()
        width_col = 180/(len(all_data.task_signals))
        heigth_row = 180/(len(all_data.task_states)-1)
        for i in range(len(all_data.task_states[1:])):
            self.grid1.SetRowLabelValue(i, all_data.task_states[i+1])
            self.grid1.SetRowSize(i, heigth_row - 0.0000000000000000000001)
        for i in range(len(all_data.task_signals)):
            self.grid1.SetColLabelValue(i, all_data.task_signals[i])
            self.grid1.SetColSize(i, width_col - 0.0000000000000000000001)
        self.grid1.SetColLabelSize(20)
        self.grid1.SetRowLabelSize(20)
        self.grid1.SetSize((200, 200))
        self.grid1.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.OnSelectCellGrid1)
        self.grid1.Bind(wx.EVT_KEY_DOWN, self.OnClickKeyGrid1)
        if all_data.step1_convolution_table != []:
            for i in range(len(all_data.step1_convolution_table)):
                for j in range(len(all_data.step1_convolution_table[i])):
                    self.grid1.SetCellValue(i, j, all_data.step1_convolution_table[i][j])

        self.grid2 = wx.grid.Grid(panel, pos=(240, 40))
        self.grid2.CreateGrid(10, 3)
        self.grid2.DisableDragGridSize()
        heigth_row = 180 / 10
        self.grid2.SetColLabelValue(0, "Исходное\nсостояние")
        self.grid2.SetColLabelValue(1, "Воздействующий\nсигнал")
        self.grid2.SetColLabelValue(2, "Конечное\nсостояние")
        self.grid2.SetColSize(0, 70)
        self.grid2.SetColSize(1, 110)
        self.grid2.SetColSize(2, 70)
        for i in range(10):
            self.grid2.SetRowLabelValue(i, str(i+1))
            self.grid2.SetRowSize(i, heigth_row)
        self.grid2.SetColLabelSize(30)
        self.grid2.SetRowLabelSize(20)
        self.grid2.SetSize((287, 200))
        self.grid2.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.OnSelectCellGrid2)
        self.grid2.Bind(wx.EVT_KEY_DOWN, self.OnClickKeyGrid2)

        if all_data.step1_convolution_table_status:
            self.grid1.Enable(False)
            self.button1.Enable(False)
        else:
            self.grid2.Enable(False)
            self.button2.Enable(False)
        if all_data.step1_jump_table != []:
            for i in range(len(all_data.step1_jump_table)):
                if self.grid2.GetNumberRows()-1 == i:
                    self.grid2.InsertRows(i+1)
                self.grid2.SetCellValue(i, 0, all_data.step1_jump_table[i][0])
                self.grid2.SetCellValue(i, 1, all_data.step1_jump_table[i][1])
                self.grid2.SetCellValue(i, 2, all_data.step1_jump_table[i][2])
        if all_data.step1_jump_table_status:
            self.grid2.EnableEditing(False)
            self.button2.Enable(False)


        self.Bind(wx.EVT_CLOSE, self.save_data)

    def OnClickButton1(self, event):
        self.save_data()
        table = {i:{j:"-" for j in all_data.task_signals} for i in all_data.task_states[1:]}
        for i in all_data.task_rules:
            if i[0] != 'H':
                table[i[0]][i[1]] = i[2] if table[i[0]][i[1]] == '-' else table[i[0]][i[1]]+i[2]

        # for i in table:
        #     print(i,':',table[i])

        states = all_data.task_states[1:]
        signals = all_data.task_signals
        flag = True
        errors.text.Clear()
        for i in range(len(states)):
            for j in range(len(signals)):
                if set(table[states[i]][signals[j]]) != set(self.grid1.GetCellValue(i,j)):
                    flag = False
                    errors.text.write("Неверное значение! (%d, %d)\n"%(i,j))
        if flag==True:
            all_data.step1_convolution_table_status = True
            self.grid1.Enable(False)
            self.button1.Enable(False)
            self.button2.Enable()
            self.grid2.Enable()

    def OnClickButton2(self, event):
        errors.text.Clear()
        self.save_data()
        lst = set(all_data.task_rules)
        # for i in lst:
        #     print(i)
        #
        # print("\n\n")
        lst_del = []    #список пустых строк, которые будут удалены
        flag = [True for i in range(self.grid2.GetNumberRows())]
        for i in range(self.grid2.GetNumberRows()):
            t = (self.grid2.GetCellValue(i,0), self.grid2.GetCellValue(i,1), self.grid2.GetCellValue(i,2))
            if (t[0] == '' or t[1] == '' or t[2] == ''):
                if (t[0] == '' and t[1] == '' and t[2] == ''):
                    lst_del.append(i)
                else:
                    flag[i] = False
            else:
                if t in all_data.task_rules:
                    lst.remove(t)
                else:
                    flag[i] = False
        for i in range(-len(lst_del)+1,1):
            self.grid2.DeleteRows(lst_del[-i])
            flag.pop(lst_del[-i])

        if (self.grid2.GetNumberRows()==0):
            self.grid2.InsertRows(self.grid2.GetNumberRows())

        for i in range(len(flag)):
            if flag[i] == False:
                errors.text.write('Некорректное значение в строке №' + str(i + 1)+'\n')

        #print(lst)
        if len(lst) == 0:
           # print("успех")
            all_data.step1_jump_table_status = True
            self.grid2.EnableEditing(False)
            self.button2.Enable(False)
            GraphWindow(all_data.graph_svg1)
            Step2Window()
        else:
            errors.text.write("Определены не все переходы.\n")

    def OnSelectCellGrid1(self, event):
        self.SelectCell1 = (event.GetRow(),event.GetCol())

    def OnSelectCellGrid2(self, event):
        self.SelectCell2 = (event.GetRow(),event.GetCol())

    def OnClickKeyGrid1(self, event):
        key = event.GetKeyCode()
        if key == wx.WXK_TAB:
            if self.SelectCell1[0] == (self.grid1.GetNumberRows() - 1) and self.SelectCell1[1] == (self.grid1.GetNumberCols() - 1):
                self.grid1.SetGridCursor(row=0, col=0)
            elif self.SelectCell1[1] == (self.grid1.GetNumberCols() - 1):
                self.grid1.SetGridCursor(row=self.SelectCell1[0]+1, col=0)
            else:
                event.Skip()
        else:
            event.Skip()

    def OnClickKeyGrid2(self, event):
        key = event.GetKeyCode()
        if key == wx.WXK_DOWN:
            if self.SelectCell2[0] == (self.grid2.GetNumberRows()-1) and all_data.step1_jump_table_status==False:
                self.grid2.InsertRows(self.grid2.GetNumberRows())
                self.grid2.SetGridCursor(row=self.grid2.GetNumberRows() - 1, col=0)
        elif key == wx.WXK_TAB:
            if self.SelectCell2[0] == (self.grid2.GetNumberRows()-1) and all_data.step1_jump_table_status==False:
                self.grid2.InsertRows(self.grid2.GetNumberRows())

            if self.SelectCell2[1] == 2:
                self.grid2.SetGridCursor(row=self.SelectCell2[0]+1,col=0)
            else:
                event.Skip()
        elif key == 13 or key == 370:
            if self.SelectCell2[0] == (self.grid2.GetNumberRows()-1) and all_data.step1_jump_table_status==False:
                self.grid2.InsertRows(self.grid2.GetNumberRows())
                event.Skip()
        else:
            event.Skip()

    def save_data(self,event=None):
        all_data.saved=False
        if all_data.step1_convolution_table_status == False:
            all_data.step1_convolution_table = [[self.grid1.GetCellValue(i,j) for j in range(self.grid1.GetNumberCols())] for i in range(self.grid1.GetNumberRows())]
        if all_data.step1_jump_table_status == False:
            all_data.step1_jump_table = [[self.grid2.GetCellValue(i,j) for j in range(3)] for i in range(self.grid2.GetNumberRows())]
        if event != None:
            wnd.menu_step.Enable(7, True)
            window_manager.del_window(self.id)
            event.Skip()

class TestsWindow(wx.MDIChildFrame):
    def __init__(self, parent):
        wx.MDIChildFrame.__init__(self, parent, title = "Шаг 0: Тест", size = (400,450))
        self.SetIcon(icon)
        all_data.Test_True_questions = 0
        all_data.Test_False_questions = 0
        self.questions = []     #Список для хранени вопросов теста
        q = questions.copy()    #копирую полный список вопросов, т.к. буду удалять вопросы, которые уже выбраны для выдачи
        for key, value in q.items():   #Выбираю вопросы
            questions2 = list(value['Вопросы'])
            for i in range(value['Выдавать']):
                self.questions.append(questions2.pop(random.randint(0,len(questions2)-1)))
                for j in range(len(self.questions[-1]['Ответы'])):      #перемешиваю варианты ответов
                    z = random.randint(0,len(self.questions[-1]['Ответы'])-1)
                    self.questions[-1]['Ответы'][j], self.questions[-1]['Ответы'][z] = self.questions[-1]['Ответы'][z], self.questions[-1]['Ответы'][j]

        for i in range(len(self.questions)):     #перемешиваю вопросы
            j = random.randint(0,len(self.questions)-1)
            self.questions[i], self.questions[j] = self.questions[j], self.questions[i]

        self.panel = wx.Panel(self)
        self.number_question_label = wx.StaticText(self.panel,label = "Вопрос 1 из %d"%(len(self.questions),), pos = (160,10), style=wx.ST_NO_AUTORESIZE)
        self.question = wx.StaticText(self.panel,label = self.questions[0]['Вопрос'], pos = (10,30), size=(380, 40), style=wx.ST_NO_AUTORESIZE)
        self.answers = []
        self.number_question = 1
        j=0
        for i in self.questions[0]['Ответы']:
            self.answers.append(wx.CheckBox(self.panel,label = i[0], pos = (10,60+j*40), size=(370, 30)))
            self.answers[-1].SetToolTip(i[0])
            j+=1
        self.button = wx.Button(self.panel,label='Дальше', pos = (160,60+j*40), size=(80, 20))
        self.button.Bind(wx.EVT_BUTTON, self.OnClick)
        self.button.SetDefault()
        self.panel.SetFocus()
        self.EnableCloseButton(False)
        self.Bind(wx.EVT_SIZE, self.Resize)

    def Resize(self, event):
        width, height = event.Size
        for i in self.answers:
            i.SetSize((width-20, 20))
        self.button.SetPosition(((width) / 2 - 40, 60+len(self.answers)*40))
        event.Skip()

    def OnClick(self, event):
        f = True
        number_answer = 0
        for i in self.answers:
            f = f & (i.GetValue() == self.questions[self.number_question-1]['Ответы'][number_answer][1])
            number_answer += 1
        if f:
            all_data.Test_True_questions += 1
        else:
            all_data.Test_False_questions += 1
        self.panel.Destroy()
        if self.number_question < len(self.questions):
            size = self.GetSize()
            self.panel = wx.Panel(self, size=size)
            self.number_question += 1
            self.number_question_label = wx.StaticText(self.panel, label="Вопрос %d из %d"%(self.number_question,len(self.questions)), pos=(160, 10), style=wx.ST_NO_AUTORESIZE)
            self.question = wx.StaticText(self.panel, label=self.questions[self.number_question-1]['Вопрос'], pos=(10, 30), size=(380, 40),
                                     style=wx.ST_NO_AUTORESIZE)
            self.answers = []
            j = 0
            for i in self.questions[self.number_question-1]['Ответы']:
                self.answers.append(
                    wx.CheckBox(self.panel, label=i[0], pos=(10, 60 + j * 40), size=(370, 30)))
                self.answers[-1].SetToolTip(i[0])
                j += 1
            if self.number_question == len(self.questions):
                self.button = wx.Button(self.panel, 101, label='Завершить', pos=(self.GetSize()[0]/ 2 - 40, 60 + j * 40), size=(80, 20))
            else:
                self.button = wx.Button(self.panel, 101, label='Дальше', pos=(self.GetSize()[0]/ 2 - 40, 60 + j * 40), size=(80, 20))
            self.button.Bind(wx.EVT_BUTTON, self.OnClick)
            self.button.SetDefault()
            self.panel.SetFocus()
        else:
            all_data.saved=False
            TestResultWindow()
            wnd.menu_file.Enable(0, True)
            wnd.menu_file.Enable(601, True)
            wnd.menu_file.Enable(2, True)
            wnd.menu_file.Enable(3, True)
            (all_data.task_states, all_data.task_signals, all_data.task_rules), all_data.task_all = task_generation(5, 4,random.randint(10,15))
            TaskWindow()
            s = Step1Window()
            self.Destroy()

class Help(wx.MDIChildFrame):
    def __init__(self, name, data):
        wx.Frame.__init__(self, wnd, title = name, size = (600,600))
        self.SetIcon(icon)
        self.h = WebView.New(self)
        self.h.EnableContextMenu(False)
        self.h.EnableHistory(False)
        f = open("temp.html", 'w', encoding='utf8')
        f.write(data)
        f.close()

        self.h.LoadURL(os.getcwd().replace('\\', '/') + "/temp.html")

class MainWindow(wx.MDIParentFrame):
    def __init__(self):
        wx.MDIParentFrame.__init__(self, None, title = "Лабораторная работа: Грамматики", size = (600,600))
        self.Bind(wx.EVT_CLOSE, self.OnClickExitProject)
        self.Show()
        self.SetIcon(icon)

        self.menu_file = wx.Menu()  # создаём экземпляр меню
        self.menu_file.Append(0, "Новый проект\tCtrl+N", "Нажмите, чтобы создать новый проект")  # добавляем подпункты к меню
        self.menu_file.Append(601, "Открыть проект\tCtrl+O", "Нажмите, чтобы открыть существующий проект")
        self.menu_file.Append(2, "Сохранить\tCtrl+S", "Нажмите, чтобы сохранить проект")
        self.menu_file.Append(3, "Сохранить как", "Нажмите, чтобы сохранить проект с указанием места сохранения")
        self.menu_file.Append(4, "Выход\tAlt+f4", "Нажмите, чтобы выйти из программы")
        self.menu_file.Enable(2, False)
        self.menu_file.Enable(3, False)

        self.menu_step = wx.Menu()
        self.menu_step.Append(5, "Шаг 0: Тест", "Нажмите, чтобы посмотреть результаты теста")
        self.menu_step.Append(6, "Задание", "Нажмите, чтобы посмотреть задание")
        self.menu_step.Append(7, "Шаг 1: Таблицы свёрток и переходов", "Нажмите, чтобы перейти к выполнению/просмотру шага 1")
        self.menu_step.Append(8, "Граф недетерминированного автомата", "Нажмите, чтобы перейти к просмотру графа недетерминированного автомата")
        self.menu_step.Append(9, "Шаг 2: Недетерминированный автомат", "Нажмите, чтобы перейти к выполнению/просмотру шага 2")
        self.menu_step.Append(10, "Шаг 3: Детерминированный автомат", "Нажмите, чтобы перейти к выполнению/просмотру шага 3")
        self.menu_step.Append(11, "Граф детерминированного автомата", "Нажмите, чтобы перейти к просмотру графа детерминированного автомата")
        self.menu_step.Append(12, "Шаг 4: Описание детерминированного автомата",
                              "Нажмите, чтобы перейти к выполнению/просмотру шага 4")

        for i in range(5,13,1):
            self.menu_step.Enable(i, False)

        self.menu_help = wx.Menu()
        self.menu_help.Append(100, 'Руководство пользователя')
        self.menu_help.Append(101, 'Теория к лабораторной работе')
        self.menu_help.Append(102, 'О программе')

        self.bar = wx.MenuBar()  # создаём рабочую область для меню
        self.bar.Append(self.menu_file, "Файл")  # добавляем пункт меню
        self.bar.Append(self.menu_step, "Шаги")
        self.bar.Append(self.menu_help, "О программе")
        self.SetMenuBar(self.bar)  # указываем, что это меню надо показать в нашей форме

        self.Bind(wx.EVT_MENU, self.OnClickNewProject, id=0)
        self.Bind(wx.EVT_MENU, self.OnClickOpenProject, id=601)
        self.Bind(wx.EVT_MENU, self.OnClickSaveProject, id=2)
        self.Bind(wx.EVT_MENU, self.OnClickSaveAsProject, id=3)
        self.Bind(wx.EVT_MENU, self.OnClickExitProject, id=4)
        self.Bind(wx.EVT_MENU, self.OnClickStep0, id=5)
        self.Bind(wx.EVT_MENU, self.OnClickTask, id=6)
        self.Bind(wx.EVT_MENU, self.OnClickStep1, id=7)
        self.Bind(wx.EVT_MENU, self.OnClickGraph1, id=8)
        self.Bind(wx.EVT_MENU, self.OnClickStep2, id=9)
        self.Bind(wx.EVT_MENU, self.OnClickStep3, id=10)
        self.Bind(wx.EVT_MENU, self.OnClickGraph2, id=11)
        self.Bind(wx.EVT_MENU, self.OnClickStep4, id=12)
        self.Bind(wx.EVT_MENU, self.OnClickGuide, id=100)
        self.Bind(wx.EVT_MENU, self.OnClickTheory, id=101)
        self.Bind(wx.EVT_MENU, self.onAboutDlg, id=102)

    def OnClickNewProject(self, event):
        if all_data.saved==True:

            window_manager.close_all_windows()
            flag=True
            while (all_data.FIO.replace(" ", "")=="") or flag:
                dlg = wx.TextEntryDialog(self, 'Введите ваше ФИО', 'Грамматики')
                if dlg.ShowModal() == wx.ID_OK:
                    if dlg.GetValue().replace(" ", "") != "":
                        all_data.reset()
                        all_data.FIO = dlg.GetValue()
                        flag = False
                        while (all_data.Group.replace(" ", "") == ""):
                            dlg = wx.TextEntryDialog(self, 'Введите вашу группу', 'Грамматики', style=wx.OK)
                            if dlg.ShowModal() == wx.ID_OK:
                                all_data.Group = dlg.GetValue()
                                dlg.Destroy()
                                if all_data.Group.replace(" ", "") != "":
                                    break
                                    break
                            dlg.Destroy()

                        wnd.SetTitle("Лабораторная работа: Грамматики    Студент: %s,   Группа: %s" % (all_data.FIO, all_data.Group))
                        for i in range(5, 12, 1):
                            self.menu_step.Enable(i, False)
                        self.menu_file.Enable(0, False)
                        self.menu_file.Enable(601, False)
                        self.menu_file.Enable(2, False)
                        self.menu_file.Enable(3, False)
                        TestsWindow(self)
                else:
                    dlg.Destroy()
                    wx.MessageDialog(parent=self,
                                     message="Создание нового проекта отменено.",
                                     caption="Создание проекта", style=wx.OK, pos=wx.DefaultPosition).ShowModal()
                    break
                dlg.Destroy()


        else:
            dlg = wx.MessageDialog(parent=self, message="Сохранения в текущем проекте не сохранены. Вы действительно хотите создать новый проект?",
                                 caption="Новый проект",style=wx.OK|wx.CANCEL,pos=wx.DefaultPosition)
            retCode = dlg.ShowModal()
            if retCode==wx.ID_OK:
                self.ClientWindow.Close()
                window_manager.close_all_windows()
                all_data.reset()
                flag = True
                while (all_data.FIO.replace(" ", "") == "") and flag:
                    dlg = wx.TextEntryDialog(self, 'Введите ваше ФИО', 'Грамматики')
                    result = dlg.ShowModal()
                    if result == wx.ID_OK:
                        all_data.FIO = dlg.GetValue()
                        while (all_data.Group.replace(" ", "") == ""):
                            dlg = wx.TextEntryDialog(self, 'Введите вашу группу', 'Грамматики', style=wx.OK)
                            if dlg.ShowModal() == wx.ID_OK:
                                all_data.Group = dlg.GetValue()
                                dlg.Destroy()
                                break
                                break
                        dlg.Destroy()
                        flag = False

                        wnd.SetTitle("Лабораторная работа: Грамматики    Студент: %s,   Группа: %s" % (all_data.FIO, all_data.Group))
                        for i in range(5, 12, 1):
                            self.menu_step.Enable(i, False)
                        self.menu_file.Enable(0, False)
                        self.menu_file.Enable(601, False)
                        self.menu_file.Enable(2, False)
                        self.menu_file.Enable(3, False)
                        TestsWindow(self)
                    else:
                        dlg.Destroy()
                        wx.MessageDialog(parent=self,
                                         message="Создание нового проекта отменено.",
                                         caption="Создание проекта", style=wx.OK, pos=wx.DefaultPosition).ShowModal()
                        break
                    dlg.Destroy()

    def OnClickOpenProject(self, event):
        if all_data.saved == True:
            self.dirname = " "
            dlgopen = wx.FileDialog(self, "Выберите файл для открытия", self.dirname, " ", "*.gsf", wx.FLP_OPEN)  # создаём диалог
            if dlgopen.ShowModal() == wx.ID_OK:  # при выборе файла
                self.filename = dlgopen.GetFilename()  # ловим название файла
                self.dirname = dlgopen.GetDirectory()  # и папку, в которой он находится
                window_manager.close_all_windows()
                for i in range(8, 12, 1):
                    self.menu_step.Enable(i, False)
                all_data.open(dlgopen.GetPath())  # открываем файл
                all_data.filename=dlgopen.GetPath()
                self.menu_file.Enable(0, True)
                self.menu_file.Enable(601, True)
                self.menu_file.Enable(2, True)
                self.menu_file.Enable(3, True)
        else:
            dlg = wx.MessageDialog(parent=self,
                                   message="Сохранения в текущем проекте не сохранены. Вы действительно хотите открыть другой проект?",
                                   caption="Открыть проект", style=wx.OK | wx.CANCEL, pos=wx.DefaultPosition)
            retCode = dlg.ShowModal()
            if retCode == wx.ID_OK:
                self.ClientWindow.Close()
                window_manager.close_all_windows()

                self.dirname = " "
                dlgopen = wx.FileDialog(self, "Выберите файл для открытия", self.dirname, " ", "*.gsf", wx.FLP_OPEN) # создаём диалог
                if dlgopen.ShowModal() == wx.ID_OK: # при выборе файла
                    self.filename = dlgopen.GetFilename() # ловим название файла
                    self.dirname = dlgopen.GetDirectory() # и папку, в которой он находится
                    for i in range(8, 12, 1):
                        self.menu_step.Enable(i, False)
                    all_data.open(os.path.join(dlgopen.GetPath())) # открываем файл
                    all_data.filename = os.path.join(dlgopen.GetPath())
                    self.menu_file.Enable(0, True)
                    self.menu_file.Enable(601, True)
                    self.menu_file.Enable(2, True)
                    self.menu_file.Enable(3, True)

    def OnClickSaveProject(self, event):
        if all_data.saved==False:
            if all_data.filename=="":
                self.OnClickSaveAsProject(None)
            else:
                all_data.save(all_data.filename)

    def OnClickSaveAsProject(self, event):
        self.dirname = all_data.filename
        dlgsave = wx.FileDialog(self, "Выберите место сохранения файла", self.dirname, " ", "*.gsf",
                                style=wx.FD_OVERWRITE_PROMPT | wx.FD_SAVE)  # создаём диалог
        if dlgsave.ShowModal() == wx.ID_OK:
            all_data.save(dlgsave.GetPath())
            all_data.filename=dlgsave.GetPath()

    def OnClickExitProject(self, event):
        if all_data.saved==False:
            dlg = wx.MessageDialog(parent=self,
                                   message="Изменения в проекте не сохранены. Вы хотите сохранить проект?",
                                   caption="Закрытие проекта", style=wx.YES_NO | wx.CANCEL, pos=wx.DefaultPosition)
            retCode = dlg.ShowModal()
            if retCode==wx.ID_YES:
                if all_data.filename=="":
                    self.dirname = all_data.filename
                    dlgsave = wx.FileDialog(self, "Выберите место сохранения файла", self.dirname, " ", "*.gsf",
                                            style=wx.FD_OVERWRITE_PROMPT | wx.FD_SAVE)  # создаём диалог
                    if dlgsave.ShowModal() == wx.ID_OK:
                        all_data.save(dlgsave.GetPath())
                        self.Destroy()
                else:
                    all_data.save(all_data.filename)
                    self.Destroy()
            elif retCode==wx.ID_NO:
                self.Destroy()
        else:
            self.Destroy()

    def OnClickStep0(self, event):
        TestResultWindow()
        self.menu_step.Enable(5, False)

    def OnClickTask(self, event):
        TaskWindow()
        self.menu_step.Enable(6, False)

    def OnClickStep1(self, event):
        Step1Window()
        self.menu_step.Enable(7, False)

    def OnClickGraph1(self, event):
        GraphWindow(all_data.graph_svg1)
        self.menu_step.Enable(8, False)

    def OnClickStep2(self, event):
        Step2Window()
        self.menu_step.Enable(9, False)

    def OnClickStep3(self, event):
        Step3Window()
        self.menu_step.Enable(10, False)

    def OnClickGraph2(self, event):
        GraphWindow(all_data.graph_svg2,flag=False)
        self.menu_step.Enable(11, False)

    def OnClickStep4(self, event):
        Step4Window()
        self.menu_step.Enable(12, False)

    def OnClickGuide(self, event):
        Help("Руководство пользователя", helps.guide)

    def OnClickTheory(self, event):
        Help("Теория к лабораторной работе", helps.theory)

    def onAboutDlg(self, event):
        aboutInfo = wx.adv.AboutDialogInfo()
        aboutInfo.SetName("Грамматики")
        aboutInfo.SetVersion("2.2")
        aboutInfo.SetDescription(("Программная модель разработана студентами\nкафедры ЭВМ ВятГУ."),)
        aboutInfo.SetCopyright("(C) 2019")
        aboutInfo.AddDeveloper("Cмольников А.Н.")
        aboutInfo.AddDeveloper("Туварев В.А.")
        wx.adv.AboutBox(aboutInfo)

def main():
    app = wx.App()
    global icon
    icon = wx.Icon(os.path.basename(__file__)[:-2]+"exe", wx.BITMAP_TYPE_ICO)
    global wnd
    wnd = MainWindow()
    global errors
    errors = ErrorsWindow()

    # (all_data.task_states, all_data.task_signals, all_data.task_rules), all_data.task_all = task_generation(5, 5,
    #                                                                                                         random.randint(
    #                                                                                                            10, 15))
    # Step1Window()
    # Step2Window()
    # GraphWindow(all_data.graph_svg1)
    # Step3Window()
    # print(all_data.task_states)
    app.MainLoop()

if __name__ == '__main__':
    main()